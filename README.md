# Ricochet Robots
Ricochet Robots is a multiplayer puzzle game published by Rio Grande Games (more on [Wikipedia](https://en.wikipedia.org/wiki/Ricochet_Robot)). This application enables to play to this game against an artificial intelligence.

This project has been carried out as part of our studies. The goal was to develop a game making in particular use of the articifial intelligence concepts learned during the semester.

Two main AI engines have been implemented, one based on the A\* algorithm and the other on Q-learning. Several difficulties are available for the former engine, each difficulty being associated to a different A\* heuristic function.

![Imgur Image](https://i.imgur.com/bqyUL3Z.png)

## Technologies
The main technologies used in this project are:
* C++
* Qt/QML

## Getting started
### Prerequisites
* Windows is recommended. The application should also run on Linux and macOS but has not been tested on those platforms.
* MinGW/GCC 7.3 or above.
* Qt 5.12.3 or above.

### Building
Windows:
```cmd
cd src
qmake -config release
mingw32-make
```

Linux & macOS:
```sh
cd src
qmake -config release
make
```

### Running
Windows:
```cmd
cd release
RicochetRobots.exe
```

Linux & macOS:
```sh
./RicochetRobots
```

### Creating a Windows application package
After having built the application, a package containing all the required Qt dependencies can be created on Windows.
```cmd
mkdir package
xcopy release/RicochetRobots.exe package
cd package
windeployqt RicochetRobots.exe --qmldir ..
```

## Gallery
<img src="https://i.imgur.com/9tz1s1a.png" width="33%"/>
<img src="https://i.imgur.com/bqyUL3Z.png" width="33%"/>
<img src="https://i.imgur.com/QyYgDHS.png" width="33%"/>

## Authors
* ALTENBACH Thomas - @taltenba
* PEREIRA Jacques - @jp.jacques.pereira
* ROBITAILLE Aymeric - @AyRobi
