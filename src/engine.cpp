#include <engine.h>
#include <astarsolver.h>
#include <qlearningsolver.h>

using namespace Game;

Engine::Engine(QObject *parent)
    : QObject(parent), mBoard(), mBoardGenerator(), mCurrentMission(),
      mUserPlayer(), mAiSolver(), mScore(), mMissionCount(), mMissionDuration(),
      mFinishedMissionCount()
{
    // Empty
}

void Engine::newGame(int missionCount, int missionDuration, Solver::Algorithm ai, Solver::DifficultyLevel difficulty)
{
    if (mAiSolver != nullptr)
    {
        mAiSolver->interrupt();
        delete mAiSolver;
        mAiSolver = nullptr;
    }

    mMissionCount = missionCount;
    mMissionDuration = missionDuration;
    mFinishedMissionCount = 0;
    mScore.reset();

    switch (ai) {
        case Solver::Algorithm::A_STAR:
            mAiSolver = new AStarSolver(this);
            break;
        case Solver::Algorithm::Q_LEARNING:
            mAiSolver = new QLearningSolver(this);
            break;
    }

    mAiSolver->setSelectedDifficultyLevel(difficulty);
    mAiSolver->setMaxThinkDelay(mMissionDuration);

    mBoardGenerator.randomize(mBoard);

    emit newGameStarted();

    nextMission();
}

bool Engine::nextMission()
{
    if (mFinishedMissionCount == mMissionCount)
        return false;

    if (mCurrentMission != nullptr) {
        mCurrentMission->abort();

        delete mCurrentMission;
        delete mUserPlayer;
    }

    mCurrentMission = new Mission(MissionContext::createRandom(mBoard), mMissionDuration, this);
    mUserPlayer = new Player(mCurrentMission->getContext(), this);

    connect(mCurrentMission, SIGNAL(missionOver(bool)), this, SLOT(onMissionOver(bool)));

    mCurrentMission->start();

    emit newMissionStarted();

    mAiSolver->solve(mCurrentMission->getContext());

    return true;
}

void Engine::completeMission()
{
    Q_ASSERT(mCurrentMission != nullptr);
    mCurrentMission->complete();
}

void Engine::stop()
{
    if (mCurrentMission == nullptr)
        return;

    mCurrentMission->abort();
}

int Engine::getMissionCount() const
{
    return mMissionCount;
}

int Engine::getMissionDuration() const
{
    return mMissionDuration;
}

int Engine::getFinishedMissionCount() const
{
    return mFinishedMissionCount;
}

Board const& Engine::getBoard() const
{
    return mBoard;
}

Mission* Engine::getCurrentMission() const
{
    return mCurrentMission;
}

Player* Engine::getUserPlayer() const
{
    return mUserPlayer;
}

Solver const* Engine::getAi() const
{
    return mAiSolver;
}

Score const& Engine::getScore() const
{
    return mScore;
}

/*QVariantList Engine::buildAISolutionStates(QVector<Move> const& aiSolution) const
{
    QVariantList aiSolutionStates;

    State curState = mCurrentMission->getContext().getInitState();
    aiSolutionStates.append(QVariant::fromValue(curState));

    foreach (Move move, aiSolution) {
        curState = curState.moveRobot(mBoard, move);
        aiSolutionStates.append(QVariant::fromValue(curState));
    }

    return aiSolutionStates;
}*/

QVariantList Engine::wrapSolution(QVector<State> const& solution) const
{
    QVariantList wrapped;
    wrapped.reserve(solution.size());

    foreach (State state, solution)
        wrapped.append(QVariant::fromValue(state));

    return wrapped;
}

void Engine::onMissionOver(bool wasAborted)
{
    mAiSolver->interrupt();

    if (wasAborted)
        return;

    QVector<State> aiSolution = mAiSolver->getSolution();
    QVector<State> userSolution = mUserPlayer->getBestSolution();
    MissionResult result;

    if (aiSolution.length() == userSolution.length())
    {
        result = MissionResult::DRAW;
    }
    else if (userSolution.isEmpty() || (!aiSolution.isEmpty() && aiSolution.length() < userSolution.length()))
    {
        mScore.addPoint(Score::Player::COMPUTER);
        result = MissionResult::COMPUTER_WINS;
    }
    else
    {
        mScore.addPoint(Score::Player::USER);
        result = MissionResult::USER_WINS;
    }

    ++mFinishedMissionCount;

    emit missionOver(wrapSolution(aiSolution), result);

    if (mFinishedMissionCount == mMissionCount)
    {
        emit gameOver();
        return;
    }
}
