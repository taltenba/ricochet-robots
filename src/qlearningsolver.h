/*********************************************************************************
*     File Name           :     QLearning.h
*     Last Modified       :     [2019-06-16 22:53]
**********************************************************************************/

#ifndef QLEARNINGSOLVER_H
#define QLEARNINGSOLVER_H

#include <functional>
#include <cmath>
#include <QVector>
#include <QHash>
#include <QSet>
#include <QLinkedList>
#include <iostream>
#include <limits>

#include "solver.h"
#include "board.h"
#include "state.h"
#include "target.h"
#include "robot.h"

//// QLearning formula variables
//#define ALPHA 0.2f // Learning rate
//#define GAMMA 0.3f // Discount factor
//
//#define EPSILON 15 // Epsilon-greedy factor out of 100
//#define EPSILON_EPISODE_ACTIVATION 50
//
//#define CHOSE_RIGHT_ROBOT_PROBABILITY 70 // Probability to chose the robot associated with the target color

// QLearning formula variables
#define ALPHA 0.2f // Learning rate
#define GAMMA 0.3f // Discount factor

//#define EPSILON 0.15 // Epsilon-greedy factor
#define EPSILON 0
#define EPSILON_EPISODE_ACTIVATION 1000

#define CHOSE_RIGHT_ROBOT_PROBABILITY 70 // Probability to chose the robot associated with the target color

#define MAX_NB_CHOICES 6000 // Less precise, but suppress loop. If you have a powerful computer, just increase this.
//6000 avec le max()

namespace Game {


class QLearningSolver : public Solver
{

public:
    static const QVector<DifficultyLevel> AVAILABLE_DIFFICULTIES;

    QLearningSolver(QObject *parent = nullptr);

    QString getSolverName();

    bool isRunning();

protected:
    void syncSolve(MissionContext const& mc, DifficultyLevel difficultyLevel);


private:

    float epsilon;

    QHash<State, QHash<State, float>> Q;

    int nbRequestedEpisodes;

    int currentEpisode;

    QVector<State> buildSolution(MissionContext const& mc);

    void generateEpisode(MissionContext const& mc);

    void generateNEpisodes(int nbEpisodes, MissionContext const& mc);

    float optimalValue(MissionContext const& mc, State* s, QHash<State, float> const& q, QSet<State>& c);

    float optimalValue(MissionContext const& mc, State* s, QHash<State, float> const& q, QSet<State>& c, int it);

    float optimalValue(MissionContext const& mc, QHash<State, float> const& q);

    void updateStateActionValue(MissionContext const& mc, State s, State sNew, QHash<State, float>& q, float a, float g, QSet<State> const& c);

    float generateReward(MissionContext const& mc, State const& sOld, State const& sNew, QSet<State> const& c);
};

}

#endif // QLEARNINGSOLVER_H
