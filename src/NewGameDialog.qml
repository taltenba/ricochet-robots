import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import com.arp.ricochetrobots.Solver 1.0

Dialog {
    title: qsTr("New game")

    readonly property alias missionCount: missionCountField.value
    readonly property alias missionDuration: missionDurationField.value
    readonly property alias aiAlgorithm: internal.aiAlgorithm
    readonly property alias aiStrength: internal.aiStrength

    QtObject {
        id: internal
        property var aiAlgorithm
        property var aiStrength
    }

    GridLayout {
        anchors.fill: parent

        Label {
            text: qsTr("Mission count:")
            Layout.row: 0
            Layout.column: 0
        }

        SpinBox {
            id: missionCountField
            from: 1
            to: 99
            value: 10
            editable: true
            Layout.row: 0
            Layout.column: 1
            Layout.fillWidth: true
        }

        Label {
            text: qsTr("Mission duration (s):")
            Layout.row: 1
            Layout.column: 0
        }

        SpinBox {
            id: missionDurationField
            from: 10
            to: 600
            value: 60
            editable: true
            Layout.row: 1
            Layout.column: 1
            Layout.fillWidth: true
        }

        Label {
            text: qsTr("AI algorithm:")
            Layout.row: 2
            Layout.column: 0
        }

        ComboBox {
            id: aiAlgorithmField
            model: ["A*", "QLearning"]
            Layout.row: 2
            Layout.column: 1
            Layout.fillWidth: true

            onCurrentTextChanged: {
                switch (currentText) {
                    case "A*":
                        internal.aiAlgorithm = Solver.A_STAR;
                        aiStrengthField.model = ["Beginner", "Medium", "Challenging", "Hard", "Insane"];
                        aiStrengthField.currentIndex = 4;
                        break;
                    case "QLearning":
                        internal.aiAlgorithm = Solver.Q_LEARNING;
                        aiStrengthField.model = ["Hard"];
                        aiStrengthField.currentIndex = 0;
                        break;
                }
            }
        }

        Label {
            text: qsTr("AI strength:")
            Layout.row: 3
            Layout.column: 0
        }

        ComboBox {
            id: aiStrengthField
            Layout.row: 3
            Layout.column: 1
            Layout.fillWidth: true

            onCurrentTextChanged: {
                switch (currentText) {
                    case "Beginner":
                        internal.aiStrength = Solver.BEGINNER;
                        break;
                    case "Medium":
                        internal.aiStrength = Solver.MEDIUM;
                        break;
                    case "Challenging":
                        internal.aiStrength = Solver.CHALLENGING;
                        break;
                    case "Hard":
                        internal.aiStrength = Solver.HARD;
                        break;
                    case "Insane":
                        internal.aiStrength = Solver.INSANE;
                        break;
                }
            }
        }
    }

    footer: DialogButtonBox {
        Button {
            text: "Start game"
            flat: true
            DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
        }
    }
}
