import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import com.arp.ricochetrobots.Score 1.0

Dialog {
    title: qsTr("Game over")
    contentWidth: 300
    contentHeight: 200

    function openDialog(score) {
        var userScore = score.getPoints(Score.USER);
        var aiScore = score.getPoints(Score.COMPUTER);

        userScoreLabel.text = userScore;
        aiScoreLabel.text = aiScore;

        if (aiScore > userScore)
            winner.text = qsTr("AI wins");
        else if (userScore < userScore)
            winner.text = qsTr("You win");
        else
            winner.text = qsTr("Draw");

        open();
    }

    Column {
        anchors.fill: parent
        anchors.centerIn: parent
        spacing: 20

        Label {
            id: winner
            anchors.left: parent.left
            anchors.right: parent.right
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            font.pixelSize: 40
        }

        Label {
            anchors.left: parent.left
            anchors.right: parent.right
            text: qsTr("Final score")
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            font.pixelSize: 24
        }

        Row {
            anchors.left: parent.left
            anchors.right: parent.right

            Label {
                width: parent.width / 2
                text: qsTr("You")
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                font.pixelSize: 20
                Layout.row: 1
                Layout.column: 0
            }

            Label {
                width: parent.width / 2
                text: qsTr("AI")
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                font.pixelSize: 20
                Layout.row: 1
                Layout.column: 1
            }
        }

        Row {
            anchors.left: parent.left
            anchors.right: parent.right

            Label {
                id: userScoreLabel
                width: parent.width / 2
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 35
                Layout.row: 2
                Layout.column: 0
                Layout.fillWidth: true
            }

            Label {
                id: aiScoreLabel
                width: parent.width / 2
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 35
                Layout.row: 2
                Layout.column: 1
                Layout.fillWidth: true
            }
        }
    }
}
