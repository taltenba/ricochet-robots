#ifndef BOARDPOS_H
#define BOARDPOS_H

#include <QObject>
#include <QPoint>
#include <iostream>

namespace Game {
    /**
     * @brief A position on the game board.
     */
    class BoardPos
    {
        Q_GADGET

        Q_PROPERTY(int x READ getX CONSTANT)
        Q_PROPERTY(int y READ getY CONSTANT)

    public:
        BoardPos();
        BoardPos(int x, int y);
        explicit BoardPos(QPoint pos);

        QPoint toPoint() const;
        int getX() const;
        int getY() const;

        BoardPos operator+(BoardPos rhs) const;
        bool operator==(BoardPos rhs) const;
        bool operator!=(BoardPos rhs) const;

        uint getHash() const;

    private:
        quint8 mPos;

        explicit BoardPos(quint8 pos);
    };

    std::ostream& operator<<(std::ostream& os, BoardPos pos);

    inline uint qHash(BoardPos key)
    {
        return key.getHash();
    }
}

Game::BoardPos operator+(Game::BoardPos lhs, QPoint const& rhs);
Game::BoardPos operator+(QPoint const& lhs, Game::BoardPos rhs);
Game::BoardPos operator-(Game::BoardPos lhs, QPoint const& rhs);
Game::BoardPos operator-(QPoint const& lhs, Game::BoardPos rhs);

Q_DECLARE_METATYPE(Game::BoardPos)

#endif // BOARDPOS_H
