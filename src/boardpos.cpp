#include <boardpos.h>

using namespace Game;

BoardPos::BoardPos() : mPos()
{
    // Empty
}

BoardPos::BoardPos(int x, int y)
{
    Q_ASSERT(x >= 0 && x < 16 && y >= 0 && y < 16);
    mPos = static_cast<quint8>(x | (y << 4));
}

BoardPos::BoardPos(QPoint pos) : BoardPos(pos.x(), pos.y())
{
    // Empty
}

BoardPos::BoardPos(quint8 pos) : mPos(pos)
{
    // Empty
}

int BoardPos::getX() const
{
    return mPos & 0x0F;
}

int BoardPos::getY() const
{
    return (mPos & 0xF0) >> 4;
}

QPoint BoardPos::toPoint() const
{
    return QPoint(getX(), getY());
}

bool BoardPos::operator==(BoardPos rhs) const
{
    return mPos == rhs.mPos;
}

bool BoardPos::operator!=(BoardPos rhs) const
{
    return mPos != rhs.mPos;
}

uint BoardPos::getHash() const
{
    return mPos;
}

BoardPos BoardPos::operator+(BoardPos rhs) const
{
    Q_ASSERT(static_cast<int>(getX()) + rhs.getX() < 16 &&
             static_cast<int>(getY()) + rhs.getY() < 16);

    return BoardPos(mPos + rhs.mPos);
}

BoardPos operator+(BoardPos lhs, QPoint const& rhs)
{
    int x = lhs.getX() + rhs.x();
    int y = lhs.getY() + rhs.y();
    return BoardPos(x, y);
}

BoardPos operator+(QPoint const& lhs, BoardPos rhs)
{
    return rhs + lhs;
}

BoardPos operator-(BoardPos lhs, QPoint const& rhs)
{
    int x = lhs.getX() - rhs.x();
    int y = lhs.getY() - rhs.y();
    return BoardPos(x, y);
}

BoardPos operator-(QPoint const& lhs, BoardPos rhs)
{
    return rhs - lhs;
}

namespace Game {
    std::ostream& operator<<(std::ostream& os, BoardPos pos)
    {
        os << "(" << pos.getX() << ", " << pos.getY() << ")";
        return os;
    }
}
