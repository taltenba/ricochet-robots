#ifndef COLOR_H
#define COLOR_H

#include <QObject>

namespace Game {
    Q_NAMESPACE

    constexpr int COLOR_COUNT = 5;

    /**
     * @brief Enumeration of the game colors.
     */
    enum class Color {
        RED = 0,
        YELLOW,
        BLUE,
        GREEN,
        MULTICOLOR
    };
    Q_ENUM_NS(Color)
};

Q_DECLARE_METATYPE(Game::Color)

#endif // COLOR_H
