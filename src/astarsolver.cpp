#include "astarsolver.h"
#include <queue>

using namespace Game;

/* Implemented difficulty levels    */
const QVector<Solver::DifficultyLevel> AStarSolver::AVAILABLE_DIFFICULTIES =
{DifficultyLevel::BEGINNER, DifficultyLevel::MEDIUM,
 DifficultyLevel::CHALLENGING, DifficultyLevel::HARD,
 DifficultyLevel::INSANE};

/*  Constructor */
AStarSolver::AStarSolver(QObject *parent) : Solver (parent)
{
    mStartCells = new QLinkedList<BoardPos>();
    setAvailableDifficultyLevels(AVAILABLE_DIFFICULTIES);
}

/*  Destructor  */
AStarSolver::~AStarSolver()
{
    delete mStartCells;
}

/*  Returns the solver name, used by the SolverStats object */
QString AStarSolver::getSolverName()
{
    return "A-Star";
}

/**
 * @brief syncSolve : synchronous solver
 * @param mc : the mission context to solve, contains a board,
 *             an initial state and a target
 * @param difficultyLevel : the difficulty level to be reached by the AI
 */
void AStarSolver::syncSolve(const MissionContext &mc, DifficultyLevel difficultyLevel)
{
    mRun.store(true);

    switch (difficultyLevel) {
    case Solver::DifficultyLevel::INSANE :
        /*  Insane mode,
         *  Uses the admissible heuristic to have an optimal solution */
        aStarAlgorithm(mc, HEURISTIC(humanHeuristic));
        break;

    case Solver::DifficultyLevel::HARD :
        /* Hard mode,
         * Uses an euclidian distance as heuristic  */
        aStarAlgorithm(mc, HEURISTIC(euclidianHeuristic));
        break;

    case Solver::DifficultyLevel::CHALLENGING :
        /* Challenging mode,
         * Uses a Manhattan distance as heuristic  */
        aStarAlgorithm(mc, HEURISTIC(manhattanHeuristic));
        break;

    case Solver::DifficultyLevel::MEDIUM :
        /* Medium mode,
         * Uses the optimal heuristic and add a random value  */
        aStarAlgorithm(mc, HEURISTIC(mediumHeuristic));
        break;

    case Solver::DifficultyLevel::BEGINNER :
        /* Beginner mode,
         * Uses a random value as heuristic  */
        aStarAlgorithm(mc, HEURISTIC(dummyHeuristic));
        break;

    default:    /*  If the difficulty level is not supported    */
        Q_ASSERT(false);
    }
}

namespace Game {
/*  Operator redefinition for the sorted set    */
bool operator<(AStarSolver::ComparableState s1, AStarSolver::ComparableState s2)
{
    return s1.fScore > s2.fScore;
}
}

/**
 * @brief aStarAlgorithm : implements the A* algorithm to find a solution according to the given mission context,
 *                         the solution can be worsen by the given heuristic
 * @param mc : the mission context (board, target, initial state)
 * @param heuristic : the heuristic to use
 */
void AStarSolver::aStarAlgorithm(const MissionContext &mc, std::function<quint8 (const Board &, State , const Target &, const Robot &)> userHeuristic)
{
    /*  Initialisation  */
    State start = mc.getInitState();    /*  Start is the initial state  */

    /*  The moving robot is the robot which has the same color as the target
     *  If the target color is multicolor then the red value is assygned by default,
     *      but estimate function will take into account the color specificity */
    Robot movingRobot = mc.getTarget().getColor() == Color::MULTICOLOR ? Robot(Color::RED) : Robot(mc.getTarget().getColor());

    ComparableState current, neighbor;
    Color color;
    Move::Direction direction;
    Node node, currentNode;

    /*  Open and close sets */
    std::priority_queue<ComparableState> *openSet = new std::priority_queue<ComparableState>(); /*  The openSet is sorted by the f(n) value */
    QSet<State> *closedSet = new QSet<State>(); /*  The closedSet is a QSet which allows fast lookup    */

    /*  For each state we store the parent and the g(n) value   */
    QHash<State, Node> *nodes = new QHash<State, Node>();

    /*  Stores the starting node    */
    node.parent = State();  /*  No parent                   */
    node.gScore = 0;        /*  initialy the g value is 0   */
    nodes->insert(start, node);

    /* Sets the heuristic to use    */
    mHeuristic = userHeuristic;

    /* Add the starting node to the open set    */
    openSet->push({start, 0});

    /* While the thread is running and the open set is not empty we iterate to find the solution    */
    while (mRun.load() && !openSet->empty()) {

        /*  Take the best candidates in the openSet */
        current = openSet->top();

        /* If the target is reached then the problem is solved, we send the solution to the main thread */
        if (isTargetReached(current.state, mc.getTarget())) {
            emit sendSolution(reconstructPath(*nodes, current.state, start));
            *openSet = std::priority_queue<ComparableState>();
            break;
        }

        /*  Add the current state in the closed set and remove it from the open set */
        closedSet->insert(current.state);
        openSet->pop();

        /*  Computes the new g value by adding the new movement */
        quint8 newGScore = (*nodes)[current.state].gScore + 1;

        /* Compute the neighbourhood
         * There are 4 robots, each can move in 4 directions    */
        for (quint8 i = 0; i < Robot::ROBOT_COUNT; ++i) {
            color = static_cast<Color>(i);

            for (quint8 j = 0; j < Move::DIRECTION_COUNT; ++j) {
                direction = static_cast<Move::Direction>(j);
                neighbor.state = current.state.moveRobot(mc.getBoard(), Move(Robot(color), direction));

                /* If the robot can't move, skip the movement  */
                if (neighbor.state == current.state)
                    continue;

                /* If the state is already computed, skip  */
                if (closedSet->contains(neighbor.state))
                    continue;

                QHash<State, Node>::const_iterator neighborNode = nodes->find(neighbor.state);

                /* If the state is in openSet and the newScore is worse than before, skip   */
                if (neighborNode != nodes->end() && newGScore >= neighborNode->gScore)
                    continue;

                /*  Computes the new f value with the heuristic    */
                neighbor.fScore = newGScore + estimate(mc, neighbor.state, movingRobot);

                /*  Add the neighbor to the openSet */
                openSet->push(neighbor);

                /* The actual state is the best until now, we store it. */
                node.parent = current.state;
                node.gScore = newGScore;
                nodes->insert(neighbor.state, node);
            }
        }
    }

    /* Free the memory  */
    delete nodes;
    delete openSet;
    delete closedSet;
}

/**
 * @brief isTargetReached : returns true if the target is reached
 * @param current : current state (contains robot positions)
 * @param target : target (contains the target's color and position)
 * @return true if the target is reached
 */
inline bool AStarSolver::isTargetReached(State current, const Target &target)
{
    return target.isReached(current); // Checks if the "good" robot is on the target, if the target is multicolor then all robot are considered as good
}

/**
 * @brief reconstructPath : reconstructs the solution in order to give a detailled solution to the user
 * @param cameFrom : the map of nodes that contains the actual graph
 * @param current : the current state
 * @param start : the starting state
 * @return a vector that contains all state (moves) to do in order to reach the target
 */
QVector<State> AStarSolver::reconstructPath(const QHash<State, Node> &cameFrom, const State &current, State start)
{
    QVector<State> inversedSolution;
    State newState = current;

    /*  Add the last sate to the inversed solution  vector  */
    inversedSolution.append(current);

    /* Iterates through the graph to find all parents
     * When the initial state is reached then the inversed solution is built    */
    while (newState != start) {
        newState = cameFrom[newState].parent;
        inversedSolution.append(newState);
    }

    /* Reorder the solution */
    QVector<State> solution(inversedSolution.size());

    for (int i = solution.size() - 1; i > -1; --i) {
        solution[i] = inversedSolution[inversedSolution.size() - i - 1];
    }

    return solution;
}

/**
 * @brief estimate : estimates the "distance" that separate a moving robot to the target
 * @param mc : the mission context (with the board, target and initial state)
 * @param state : the current state
 * @param movingRobot : the robot to move
 * @param forceAll : if true, considers that all robot are moving
 * @return the least estimated value
 */
quint8 AStarSolver::estimate(const MissionContext &mc, State state, const Robot &movingRobot, bool forceAll)
{
    /* If the target is multicolor or if the AI set forceAll to true
     * Then, considers all robot can move  and return the min value  */
    if (mc.getTarget().getColor() == Color::MULTICOLOR || forceAll) {
        quint8 min = std::numeric_limits<quint8>::max();
        quint8 res;
        Color color;

        for (quint8 i = 0; i < Robot::ROBOT_COUNT; ++i) {
            color = static_cast<Color>(i);
            res = mHeuristic(mc.getBoard(), state, mc.getTarget(), Robot(color));

            if (res < min)
                min = res;
        }

        return min;

    } else { /* Else returns the heuritic value applied to the moving robot */
        return mHeuristic(mc.getBoard(), state, mc.getTarget(), movingRobot);
    }
}

/**
 * @brief initEstimatedMoveTab : initiates the estimated board of move, this "board" is used by the humman heuristic
 */
inline void AStarSolver::initEstimatedMoveTab()
{
    memset(mEstimatedMoveBoard, std::numeric_limits<quint8>::max(), Board::HEIGHT * Board::WIDTH * sizeof (quint8));
}

/**
 * @brief getEstimatedValueFormPos : returns the actual estimated number of move according to a given position
 * @param pos : position to evaluate
 * @return the actual estimated number of move according to a given position
 */
inline quint8 AStarSolver::getEstimatedValueFormPos(const BoardPos &pos)
{
    return mEstimatedMoveBoard[pos.getY()][pos.getX()];
}

/**
 * @brief setEstimatedValueFormPos : sets the new estimated value (for the number of moves) according to a position
 * @param pos : position to set
 * @param newVal : new estimated value
 */
inline void AStarSolver::setEstimatedValueFormPos(const BoardPos &pos, quint8 newVal)
{
    mEstimatedMoveBoard[pos.getY()][pos.getX()] = newVal;
}

/**
 * @brief displayEstimatedBoard : debug function, prints the evaluated moves for the current state and mission context
 * @param o : an ostream
 */
void AStarSolver::displayEstimatedBoard(std::ostream &o)
{
    /* Prints column index (start from 0)   */
    o << " " << "\t";
    for (int j = 0; j < Board::WIDTH; ++j)
        o << j << "\t";

    o << std::endl;

    for (int i = 0; i < Board::HEIGHT; ++i) {
        o << i << "\t"; /* Prints row index */
        for (int j = 0; j < Board::WIDTH; ++j) {
            /* Prints estimated value   */
            o << static_cast<unsigned int>(mEstimatedMoveBoard[i][j]) << "\t";
        }
        o << std::endl;
    }
}

/**
 * @brief humanHeuristic : admissible heuristique to compute a minimum value for the remaining number of moves to reach the target
 * @param board : actual board
 * @param state : current state
 * @param target : target to achieve
 * @param movingRobot : robot to move
 * @return an estimated value of remaining moves, the real value is higher or same
 */
quint8 AStarSolver::humanHeuristic(const Board &board, State state, const Target &target, const Robot &movingRobot)
{
    /* This heuristic allows the robot to stop where it wants to compute a minimal number of moves
     * and not a distance between the robot and the target  */

    quint8 actualVal = 0;           /*  Actual number of moves                  */
    quint8 nextVal = actualVal + 1; /*  Number of moves for the next iteration  */
    bool notReached = true;         /*  Sets to false when target is reached    */
    Move::Direction direction;      /*  Moving direction (UP, DOWN, RIGHT, LEFT)*/

    BoardPos adjacentPos;           /*  Adjacent position according to move direction   */
    BoardPos newPos, nextPos;       /*  New ending position, next position for the move */
    Move mover;                     /*  Mover used to move a specified robot in a specified direction   */
    State newState, currentState;   /*  New state after moving, current state for a given iteration     */

    /*  List iterators  */
    QLinkedList<BoardPos>::iterator endIterator;
    QLinkedList<BoardPos>::iterator it;

    initEstimatedMoveTab();

    /*  If a robot is on the target
     *  then returns 0 if the color is good
     *  if not returns 255 (the max value of an unsigned int on 8 bits)   */
    for (quint8 i = 0; i < Robot::ROBOT_COUNT; ++i) {
        if (target.getPos() == state.getRobotPos(Robot::ROBOTS[i])) {
            if (target.getColor() == static_cast<Color>(i) || target.getColor() == Color::MULTICOLOR)
                return 0;
            else
                return std::numeric_limits<quint8>::max();
        }
    }

    mStartCells->clear(); /*    Clears the starting cells set   */

    /*  Initiates the staring cells set by the position of the moving robot */
    mStartCells->append(state.getRobotPos(movingRobot));
    setEstimatedValueFormPos(mStartCells->first(), 0);

    /* While the target is not reached and the start cells set is not empty we iterates */
    while (notReached && !mStartCells->isEmpty()) {
        endIterator = mStartCells->end();

        /*  Starts with the first starting cell
         *  and iterates while the target is notReached and starting cell are not computed for the current iteration    */
        it = mStartCells->begin();
        while (it != endIterator && notReached) {
            currentState = state.setRobotPos(movingRobot, *it);

            /* Moves the moving robot in the 4 possible direction   */
            for (quint8 dir = 0; dir < Move::DIRECTION_COUNT; ++dir) {
                direction = static_cast<Move::Direction>(dir);
                mover = Move(movingRobot, direction);

                if (!currentState.canRobotMove(board, mover)) /* if the adjacent cell is invalid, skip this move  */
                    continue;

                /* Sets the new adjacent position by moving the robot in the given direction    */
                adjacentPos = *it + mover.getVector();

                if (getEstimatedValueFormPos(adjacentPos) <= actualVal) /* if the cell is already computed, skip    */
                    continue;

                /* Gets the max position for the robot according to the move and positon of other robots and walls  */
                newState = currentState.moveRobot(board, mover);
                newPos = newState.getRobotPos(movingRobot);

                if (newPos == *it) /* if we can't move then skip    */
                    continue;

                if (newPos == target.getPos()) { /* if target reached, exit */
                    setEstimatedValueFormPos(newPos, nextVal);
                    notReached = false;
                    break;
                }

                /*  Sets the new cell values and add cells to the list  */

                nextPos = adjacentPos;

                do {
                    adjacentPos = nextPos;
                    endIterator = mStartCells->insert(endIterator, adjacentPos);
                    setEstimatedValueFormPos(adjacentPos, nextVal);
                    if (nextPos != newPos)
                        nextPos = nextPos + mover.getVector();
                } while (adjacentPos != newPos && getEstimatedValueFormPos(nextPos) > actualVal);

            }
            /*  Remove the computed cell of the list of starting cells and go to the next one   */
            it = mStartCells->erase(it);
        }

        /*  Goes to the next generation */
        ++actualVal;
        ++nextVal;
    }

    return getEstimatedValueFormPos(target.getPos());
}

/**
 * @brief abs : computes the absolute value
 * @param a : an integer
 * @return the absolute value of a
 */
inline int AStarSolver::abs(int a)
{
    if (a < 0)
        return -a;
    else
        return a;
}

/**
 * @brief manhattanHeuristic : computes the manhattan distance between the moving robot and the target
 * @param board : actual board
 * @param state : current state
 * @param target : target to achieve
 * @param movingRobot : robot to move
 * @return the manhattan distance between the moving robot and the target
 */
inline quint8 AStarSolver::manhattanHeuristic(const Board &board, State state, const Target &target, const Robot &movingRobot)
{
    Q_UNUSED(board)
    BoardPos start = state.getRobotPos(movingRobot); /* Starting position   */
    BoardPos end = target.getPos(); /* Ending position  */

    return static_cast<quint8>(abs(end.getX() - start.getX()) + abs(end.getY() - start.getY()));
}

/**
 * @brief euclidianHeuristic : computes the euclidian distance between the moving robot and the target
 * @param board : actual board
 * @param state : current state
 * @param target : target to achieve
 * @param movingRobot : robot to move
 * @return the euclidian distance between the moving robot and the target
 */
inline quint8 AStarSolver::euclidianHeuristic(const Board &board, State state, const Target &target, const Robot &movingRobot)
{
    Q_UNUSED(board)
    BoardPos start = state.getRobotPos(movingRobot); /* Starting position   */
    BoardPos end = target.getPos(); /* Ending position  */

    return static_cast<quint8>(
                std::sqrt(
                    (start.getX() - end.getX()) * (start.getX() - end.getX())
                    +
                    (start.getY() - end.getY()) * (start.getY() - end.getY())
                    )
                );
}

/**
 * @brief mediumHeuristic : computes the distance according to the humanHeuristic apply randomly a worsing value
 * @param board : actual board
 * @param state : current state
 * @param target : target to achieve
 * @param movingRobot : robot to move
 * @return the worsed value
 */
quint8 AStarSolver::mediumHeuristic(const Board &board, State state, const Target &target, const Robot &movingRobot)
{
    /* Computes an heuritic value with the optimal heuritic
     * then add  random integer in [0,100[ with a probability of 80%  */
    quint8 optimal = humanHeuristic(board, state, target, movingRobot);
    quint8 worseIt = static_cast<quint8>(rand() % 20);

    return worseIt > 3 ? optimal + static_cast<quint8>(rand() % 100) : optimal;
}

/**
 * @brief dummyHeuristic : computes a full random value
 * @param board : actual board
 * @param state : current state
 * @param target : target to achieve
 * @param movingRobot : robot to move
 * @return a full random value
 */
quint8 AStarSolver::dummyHeuristic(const Board &board, State state, const Target &target, const Robot &movingRobot)
{
    /* Computes an heuritic value with the optimal heuritic
     * then add  random integer in [0,100[ with a probability of 100%  */
    quint8 optimal = humanHeuristic(board, state, target, movingRobot);

    return optimal + static_cast<quint8>(rand() % 100);
}
