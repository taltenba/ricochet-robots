#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QTranslator>
#include <QLocale>
#include <QVariant>
#include <QMetaType>
#include <engine.h>
#include <ctime>
#include <cstdlib>
#include <QRandomGenerator>

#include <iostream>
#include <fstream>
#include "solver.h"
#include "astarsolver.h"
#include "solverstats.h"
#include "qlearningsolver.h"

//#define STATS

static QVariantList getRobots()
{
    QVariantList robots;

    for (int i = 0; i < Game::Robot::ROBOT_COUNT; ++i)
        robots.append(QVariant::fromValue(Game::Robot::ROBOTS[i]));

    return robots;
}

int main(int argc, char *argv[])
{
//    srand(static_cast<unsigned int>(time(nullptr)));
    srand(1945);

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QLocale::setDefault(QLocale::English);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    qmlRegisterUncreatableMetaObject(Game::staticMetaObject, "com.arp.ricochetrobots.GameColor", 1, 0, "GameColor",
                                     "Not intended to be created, contains only enum.");

    qmlRegisterUncreatableType<Game::Board>("com.arp.ricochetrobots.GameBoard", 1, 0, "GameBoard",
                                            "Not intended to be created from QML.");

    qmlRegisterUncreatableType<Game::BoardSquare>("com.arp.ricochetrobots.BoardSquare", 1, 0, "BoardSquare",
                                                  "Not intended to be created from QML.");

    qmlRegisterUncreatableType<Game::BoardPos>("com.arp.ricochetrobots.BoardPos", 1, 0, "BoardPos",
                                               "Not intended to be created from QML.");

    qmlRegisterUncreatableType<Game::Target>("com.arp.ricochetrobots.Target", 1, 0, "Target",
                                             "Not intended to be created from QML.");

    qmlRegisterUncreatableType<Game::Mission>("com.arp.ricochetrobots.Mission", 1, 0, "Mission",
                                              "Not intended to be created from QML.");

    qmlRegisterUncreatableType<Game::MissionContext>("com.arp.ricochetrobots.MissionContext", 1, 0, "MissionContext",
                                                     "Not intended to be created from QML.");

    qmlRegisterUncreatableType<Game::Move>("com.arp.ricochetrobots.Move", 1, 0, "Move",
                                           "Not intended to be created from QML.");

    qmlRegisterUncreatableType<Game::Player>("com.arp.ricochetrobots.Player", 1, 0, "Player",
                                             "Not intended to be created from QML.");

    qmlRegisterUncreatableType<Game::Robot>("com.arp.ricochetrobots.Robot", 1, 0, "Robot",
                                            "Not intended to be created from QML.");

    qmlRegisterUncreatableType<Game::Score>("com.arp.ricochetrobots.Score", 1, 0, "Score",
                                            "Not intended to be created from QML.");

    qmlRegisterUncreatableType<Game::State>("com.arp.ricochetrobots.State", 1, 0, "State",
                                            "Not intended to be created from QML.");

    qmlRegisterUncreatableType<Game::Solver const>("com.arp.ricochetrobots.Solver", 1, 0, "Solver",
                                                   "Not intended to be created from QML.");

    qmlRegisterType<Game::Engine>("com.arp.ricochetrobots.GameEngine", 1, 0, "GameEngine");

    qRegisterMetaType<Game::State>("State");

    qRegisterMetaType<QVector<Game::State>>("QVector<Game::State>");

    QVariantList robots = getRobots();
    engine.rootContext()->setContextProperty("ROBOTS", robots);

    /*  Start statistic section  */

#ifdef STATS
    /*
    Game::Solver *sol = new Game::AStarSolver();
    sol->setMaxThinkDelay(60000);
    Game::SolverStats stats(*sol);

    stats.randomPoolTest(300);

    sol->setSelectedDifficultyLevel(Game::Solver::DifficultyLevel::INSANE);
    stats.startSimulation();

    sol->setSelectedDifficultyLevel(Game::Solver::DifficultyLevel::HARD);
    stats.startSimulation();

    sol->setSelectedDifficultyLevel(Game::Solver::DifficultyLevel::CHALLENGING);
    stats.startSimulation();

    sol->setSelectedDifficultyLevel(Game::Solver::DifficultyLevel::MEDIUM);
    stats.startSimulation();

    sol->setSelectedDifficultyLevel(Game::Solver::DifficultyLevel::BEGINNER);
    */
    Game::Solver *sol = new Game::QLearningSolver();
    sol->setMaxThinkDelay(60000);
    sol->setSelectedDifficultyLevel(Game::Solver::DifficultyLevel::HARD);
    Game::SolverStats stats(*sol);

    stats.randomPoolTest(10);
    stats.startSimulation();

    std::ofstream fileStream;
    fileStream.open("poolTest9.csv");

    std::cout << stats << std::endl;
    fileStream << stats;

    fileStream.close();

    delete sol;
    exit(0);
#endif
    /*  Stop statistic section   */

    engine.load(url);

    return app.exec();
}
