#include <missioncontext.h>
#include <boardpos.h>

using namespace Game;

MissionContext::MissionContext() : mBoard(), mInitState(), mTarget()
{
    // Empty
}

MissionContext::MissionContext(Board const& board, State initState, Target target)
    : mBoard(&board), mInitState(initState), mTarget(target)
{
    // Empty
}

Board const& MissionContext::getBoard() const
{
    return *mBoard;
}

Target const& MissionContext::getTarget() const
{
    return mTarget;
}

State MissionContext::getInitState() const
{
    return mInitState;
}

bool MissionContext::isAccomplished(State state) const
{
    return mTarget.isReached(state);
}

bool MissionContext::isTrivial() const
{
    if (mTarget.getColor() == Color::MULTICOLOR)
    {
        for (int i = 0; i < Robot::ROBOT_COUNT; ++i)
        {
            if (canReachTargetInOneMove(Robot::ROBOTS[i]))
                return true;
        }

        return false;
    }

    return canReachTargetInOneMove(Robot(mTarget.getColor()));
}

bool MissionContext::canReachTargetInOneMove(Robot robot) const
{
    for (int i = 0; i < Move::DIRECTION_COUNT; ++i)
    {
        Move::Direction dir = static_cast<Move::Direction>(i);
        Move move(robot, dir);

        if (mTarget.isReached(mInitState.moveRobot(*mBoard, move)))
            return true;
    }

    return false;
}

MissionContext MissionContext::createRandom(Board const& board)
{
    BoardPos initRobotPos[Robot::ROBOT_COUNT];
    MissionContext ctx;

    do {
        QSet<BoardPos> usedPos;
        usedPos.insert(BoardPos(7, 7));
        usedPos.insert(BoardPos(7, 8));
        usedPos.insert(BoardPos(8, 7));
        usedPos.insert(BoardPos(8, 8));

        for (int i = 0; i < Robot::ROBOT_COUNT; ++i)
        {
            BoardPos pos = board.getRandomPos(usedPos);

            initRobotPos[i] = pos;
            usedPos.insert(pos);
        }

        State initState = State(initRobotPos);

        Color targetColor = static_cast<Color>(rand() % COLOR_COUNT);

        BoardPos targetPos = board.getRandomTargetPos(usedPos);
        ctx = MissionContext(board, initState, Target(targetPos, targetColor));
    } while (ctx.isTrivial());

    return ctx;
}
