#ifndef RANDOMBOARDGENERATOR_H
#define RANDOMBOARDGENERATOR_H

#include <QVector>
#include <QTextStream>
#include <board.h>

namespace Game {
    class RandomBoardGenerator {
    public:
        RandomBoardGenerator();

        void randomize(Board& board) const;

    private:
        struct Panel {
            static constexpr int WIDTH = 8;
            static constexpr int HEIGHT = 8;

            Panel() : mSquares(), mValidTargetPos() { /* Empty */ }

            BoardSquare mSquares[WIDTH][HEIGHT];
            QVector<BoardPos> mValidTargetPos;
        };

        QVector<Panel> mPanels;

        void readPanel(QTextStream& in, Panel& panel);

        void setBoardPanel(Board& board, Panel const& panel, int rot) const;

        BoardPos rotatePos(int x, int y, int rot) const;
    };
}

#endif // RANDOMBOARDGENERATOR_H
