#include <state.h>
#include <cstring>

using namespace Game;

State::State() : mRobotPos()
{
    // Empty
}

State::State(BoardPos robotPos[Robot::ROBOT_COUNT])
{
    std::copy(robotPos, &robotPos[Robot::ROBOT_COUNT], mRobotPos);
}

bool State::canRobotMove(Board const& board, Move move) const
{
    BoardSquare::Wall blockingWall = move.getBlockingWall();
    BoardPos pos = getRobotPos(move.getRobot());

    return !board.getSquare(pos).hasWall(blockingWall) && findRobot(pos + move.getVector()) == nullptr;
}

struct Segment
{
    Segment(int x1, int x2)
    {
        if (x1 < x2)
        {
            start = x1;
            end = x2;
        }
        else
        {
            start = x2;
            end = x1;
        }
    }

    int start;
    int end;
};

State State::moveRobot(Board const& board, Move move) const
{
    Robot robot = move.getRobot();
    BoardPos pos = getRobotPos(robot);
    QPoint moveVector = move.getVector();

    BoardPos result = board.getPrecomputedMoveResult(pos, move.getDirection());

    if (result == pos)
        return *this;

    if (pos.getX() == result.getX())
    {
        Segment seg(pos.getY(), result.getY());

        for (int i = 0; i < Robot::ROBOT_COUNT; ++i)
        {
            BoardPos otherRobotPos = getRobotPos(Robot::ROBOTS[i]);

            if (otherRobotPos == pos || otherRobotPos.getX() != pos.getX())
                continue;

            int y = otherRobotPos.getY();

            if (seg.start <= y && y <= seg.end)
            {
                result = otherRobotPos - moveVector;
                seg = Segment(pos.getY(), result.getY());
            }
        }
    }
    else
    {
        Segment seg(pos.getX(), result.getX());

        for (int i = 0; i < Robot::ROBOT_COUNT; ++i)
        {
            BoardPos otherRobotPos = getRobotPos(Robot::ROBOTS[i]);

            if (otherRobotPos == pos || otherRobotPos.getY() != pos.getY())
                continue;

            int x = otherRobotPos.getX();

            if (seg.start <= x && x <= seg.end)
            {
                result = otherRobotPos - moveVector;
                seg = Segment(pos.getX(), result.getX());
            }
        }
    }

    return setRobotPos(robot, result);
}

State State::setRobotPos(Robot robot, BoardPos const& pos) const
{
    State copy = *this;
    copy.mRobotPos[static_cast<int>(robot.getColor())] = pos;

    return copy;
}

quint32 State::getHash() const
{
    return *reinterpret_cast<quint32 const*>(mRobotPos);
}

BoardPos State::getRobotPos(Robot robot) const
{
    return mRobotPos[static_cast<int>(robot.getColor())];
}

Robot const* State::findRobot(BoardPos pos) const
{
    for (int i = 0; i < Robot::ROBOT_COUNT; ++i)
    {
        if (pos == getRobotPos(Robot::ROBOTS[i]))
            return &Robot::ROBOTS[i];
    }

    return nullptr;
}

QVariant State::findWrappedRobot(int x, int y) const
{
    Robot const* robot = findRobot(BoardPos(x, y));
    return robot == nullptr ? QVariant() : QVariant::fromValue(*robot);
}

bool State::operator==(State other)
{
    return std::memcmp(mRobotPos, other.mRobotPos, Robot::ROBOT_COUNT * sizeof(quint8)) == 0;
}

bool State::operator!=(State other)
{
    return !(*this == other);
}

namespace Game {
    std::ostream& operator<<(std::ostream& os, State state)
    {
        os << "(";

        for (int i = 0; i < Robot::ROBOT_COUNT; ++i)
            os << state.mRobotPos[i];

        os << ")";

        return os;
    }
}
