#ifndef BOARDSQUARE_H
#define BOARDSQUARE_H

#include <QObject>

namespace Game {
    /**
     * @brief A square of the game board.
     */
    class BoardSquare
    {
        Q_GADGET

    public:
        BoardSquare();
        BoardSquare(quint8 walls);

        enum class Wall : quint8 {
            BOTTOM = 1,
            LEFT = 2,
            TOP = 4,
            RIGHT = 8
        };
        Q_ENUM(Wall)

        Q_INVOKABLE
        bool hasWall(Wall wall) const;

        void addWall(Wall wall);

        void removeWall(Wall wall);

        BoardSquare rotate(int rot) const;

    private:
        quint8 mWalls;
    };
}

Q_DECLARE_METATYPE(Game::BoardSquare)
Q_DECLARE_METATYPE(Game::BoardSquare::Wall)

#endif // BOARDSQUARE_H
