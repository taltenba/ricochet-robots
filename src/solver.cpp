#include "solver.h"

using namespace Game;

Solver::Solver(QObject *parent) :
    QObject (parent),
    mRun(false),
    mThread(nullptr),
    mTimer(new QTimer(this)),
    mMaxThinkDelay(6000)
{
    mTimer->setSingleShot(true);
    connect(mTimer, SIGNAL(timeout()), this, SLOT(timeoutSlot()));
    connect(this, SIGNAL(sendSolution(QVector<Game::State>)), this, SLOT(sendSolutionSlot(QVector<Game::State>)));
}

Solver::~Solver()
{
    stopTimer();
    stopThread();

    if (mTimer != nullptr)
        mTimer->deleteLater();
}

QString Solver::getSolverName()
{
    return "Unknown";
}

void Solver::solve(const MissionContext &mc)
{
    interrupt();

    mSolution = QVector<State>();

    mRun.store(true);

    mThread = QThread::create([this, mc] {this->syncSolve(mc, mSelectedDifficultyLevel);});

    mThread->setObjectName("Solver thread");
    mThread->start();

    mTimer->start(mMaxThinkDelay);

}

void Solver::interrupt()
{
    stopTimer();
    stopThread();
}

void Solver::wait()
{
    if (mThread == nullptr)
        return;

    int maxTime;

    QTimer timer;
    timer.setSingleShot(true);
    QEventLoop loop;

    maxTime = mTimer == nullptr ? mMaxThinkDelay : mTimer->remainingTime();

    connect(mThread, SIGNAL(finished()), &loop, SLOT(quit()));
    //connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

    timer.start(maxTime);

    loop.exec();
}

QVector<Solver::DifficultyLevel> Solver::getAvailableDifficultyLevel() const
{
    return mAvailableDifficultyLevels;
}

int Solver::getMaxThinkDelay() const
{
    return mMaxThinkDelay;
}

void Solver::setMaxThinkDelay(const int &maxThinkDelay)
{
    mMaxThinkDelay = maxThinkDelay;
}

int Solver::getRemainingThinkTime() const
{
    return mTimer->remainingTime();
}

Solver::DifficultyLevel Solver::getSelectedDifficultyLevel() const
{
    return mSelectedDifficultyLevel;
}

void Solver::setSelectedDifficultyLevel(const DifficultyLevel &difficultyLevel)
{
    if (mAvailableDifficultyLevels.contains(difficultyLevel))
        mSelectedDifficultyLevel = difficultyLevel;
    else
        throw std::invalid_argument("Difficulty level not supported");
}

QVector<State> Solver::getSolution() const
{
    return mSolution;
}

int Solver::getSolutionLength() const
{
    return std::max(mSolution.size() - 1, 0);
}

bool Solver::isSolutionFound() const
{
    return !mSolution.isEmpty();
}

bool Solver::isSolving() const
{
    if (mThread == nullptr)
        return false;
    else
        return mThread->isRunning();
}

void Solver::setAvailableDifficultyLevels(const QVector<Solver::DifficultyLevel> &availableDifficultyLevels)
{
    mAvailableDifficultyLevels = availableDifficultyLevels;

    if (mAvailableDifficultyLevels.isEmpty())
        return;

    setSelectedDifficultyLevel(mAvailableDifficultyLevels[0]);
}

void Solver::sendSolutionSlot(QVector<Game::State> solution)
{
    mSolution = solution;
    stopThread();
    stopTimer();
    emit think(solution);
    emit finished(false);
}

void Solver::timeoutSlot()
{
    interrupt();
    emit finished(true);
}

void Solver::stopThread()
{
    if (mThread == nullptr)
        return;

    mRun.store(false);
    mThread->quit();
    mThread->wait();
    mThread->deleteLater();
    mThread = nullptr;
}

void Solver::stopTimer()
{
    if (mTimer == nullptr)
        return;

    mTimer->stop();
}
