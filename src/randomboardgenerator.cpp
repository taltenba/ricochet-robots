#include <randomboardgenerator.h>
#include <QFile>
#include <QTextStream>

using namespace Game;

RandomBoardGenerator::RandomBoardGenerator() : mPanels()
{
    QFile file(QStringLiteral(":/board_panels.data"));
    file.open(QIODevice::ReadOnly | QIODevice::Text);

    QTextStream in(&file);

    int count;
    in >> count;

    mPanels.resize(count);

    for (int i = 0; i < count; ++i)
        readPanel(in, mPanels[i]);

    file.close();
}

void RandomBoardGenerator::readPanel(QTextStream& in, Panel& panel)
{
    for (int i = 0; i < Panel::WIDTH; ++i)
    {
        for (int j = 0; j < Panel::HEIGHT; ++j)
        {
            int squareData;
            in >> squareData;

            if (squareData & 0xF0)
                panel.mValidTargetPos.push_back(BoardPos(j, i));

            panel.mSquares[j][i] = BoardSquare(squareData & 0x0F);
        }
    }
}

void RandomBoardGenerator::randomize(Board& board) const
{
    for (int i = 0; i < 4; ++i)
    {
        Panel const& panel = mPanels[rand() % mPanels.size()];
        setBoardPanel(board, panel, i * 90);
    }

    board.precomputeMoves();
}

void RandomBoardGenerator::setBoardPanel(Board& board, Panel const& panel, int rot) const
{
    int offsetX, offsetY;

    switch (rot) {
        case 0:
            offsetX = 0;
            offsetY = Panel::HEIGHT;
            break;
        case 90:
            offsetX = 0;
            offsetY = 0;
            break;
        case 180:
            offsetX = Panel::WIDTH;
            offsetY = 0;
            break;
        case 270:
            offsetX = Panel::WIDTH;
            offsetY = Panel::HEIGHT;
            break;
        default:
            Q_ASSERT(0);
            return;
    }

    for (int i = 0; i < Panel::WIDTH; ++i)
    {
        for (int j = 0; j < Panel::HEIGHT; ++j)
        {
            BoardPos pos = rotatePos(i, j, rot);
            BoardSquare const& square = panel.mSquares[i][j];

            board[pos.getX() + offsetX][pos.getY() + offsetY] = square.rotate(rot);
        }
    }

    QVector<BoardPos> validTargetPos;
    validTargetPos.reserve(panel.mValidTargetPos.size());

    BoardPos offsetVector(offsetX, offsetY);

    foreach (BoardPos pos, panel.mValidTargetPos)
        validTargetPos.append(offsetVector + rotatePos(pos.getX(), pos.getY(), rot));

    board.setValidTargetPos(validTargetPos);
}

BoardPos RandomBoardGenerator::rotatePos(int x, int y, int rot) const
{
    int n = Panel::WIDTH - 1;

    switch (rot) {
        case 0:
            return BoardPos(x, y);
        case 90:
            return BoardPos(n - y, x);
        case 180:
            return BoardPos(n - x, n - y);
        case 270:
            return BoardPos(y, n - x);
    }

    Q_ASSERT(0);
    return {};
}
