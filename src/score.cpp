#include <score.h>

using namespace Game;

Score::Score() : mPoints()
{
    // Empty
}

void Score::addPoint(Player player)
{
    ++mPoints[static_cast<int>(player)];
}

int Score::getPoints(Player player) const
{
    return mPoints[static_cast<int>(player)];
}

void Score::reset()
{
    std::fill(mPoints, &mPoints[PLAYER_COUNT], 0);
}
