/*********************************************************************************
*     File Name           :     qlearningsolver.cpp
*     Last Modified       :     [2019-06-16 23:16]
**********************************************************************************/

#include "qlearningsolver.h"

#include <QSet>
#include <QTimer>


QTimer* time2;

using namespace Game;

const QVector<Solver::DifficultyLevel> QLearningSolver::AVAILABLE_DIFFICULTIES = {DifficultyLevel::HARD};

QLearningSolver::QLearningSolver(QObject *parent) : Solver (parent)
{
    setAvailableDifficultyLevels(AVAILABLE_DIFFICULTIES);

    QLearningSolver::epsilon = EPSILON;
}

void QLearningSolver::syncSolve(MissionContext const& mc, DifficultyLevel difficultyLevel) {
    Q_UNUSED(difficultyLevel);

    Q.clear();

    // TEMPS DEPART

	currentEpisode = 0;
	generateNEpisodes(100000, mc);

    if (currentEpisode < 2)
        return; // Interrupted before any episode could be completed

    emit sendSolution(buildSolution(mc));

    	//std::cout << "Nous sommes à l'épisode " << currentEpisode << std::endl;
	if (currentEpisode <= 10000) {
		// Should not remove this warning
		//std::cout << "Carefull, may not have enough episodes generated" << std::endl;
	}
}

void QLearningSolver::generateNEpisodes(int nbEpisodes, MissionContext const& mc) {
	nbRequestedEpisodes = nbEpisodes;
    for (int i = 0; i < nbEpisodes && mRun.load(); ++i) {
		generateEpisode(mc);
		++currentEpisode;
	}
}

void QLearningSolver::generateEpisode(MissionContext const& mc) {

	int mvt = 0, nbChoices = 0;
	float foundState = 0, epsilonTime, epsilonNbEpisodes;

	QSet<State> closeList;

	State s = mc.getInitState(); // State
	State sNew = mc.getInitState();
	const Board b = mc.getBoard();
	const Target t = mc.getTarget();

	Robot r;
	Move::Direction dir;
	Move a; // Action


	closeList.insert(s);

	do {
		foundState = 0;
		//if (currentEpisode < EPSILON_EPISODE_ACTIVATION || rand() % 100 < 100*QLearningSolver::epsilon) { // Epsilon greedy factor
		//if (rand() % 100 > 100*currentEpisode/nbRequestedEpisodes*currentEpisode/nbRequestedEpisodes) { // Epsilon greedy factor
		//if (rand() % 100 > 100*(60000-time->remainingTime())/60000*(60000-time->remainingTime())/60000) { // Epsilon greedy factor
		epsilonTime = (getMaxThinkDelay()-Solver::getRemainingThinkTime())/getMaxThinkDelay()*(getMaxThinkDelay()-Solver::getRemainingThinkTime())/getMaxThinkDelay();
		epsilonNbEpisodes = currentEpisode/nbRequestedEpisodes*currentEpisode/nbRequestedEpisodes;
		QLearningSolver::epsilon = epsilonTime > epsilonNbEpisodes ? epsilonTime : epsilonNbEpisodes;
		if (rand() % 100 > 100*QLearningSolver::epsilon) { // Epsilon greedy factor
			if (t.getColor() == Color::MULTICOLOR || rand() % 100 > CHOSE_RIGHT_ROBOT_PROBABILITY) {
				r = Robot::ROBOTS[rand() % 4];
			} else {
				r = Robot::ROBOTS[static_cast<int> (t.getColor())];
			}
			dir = static_cast<Move::Direction> (rand() % 4);
			a = Move(r, dir);

			/* Chose a possible action */
			sNew = s.moveRobot(b, a);
		} else {
			if (QLearningSolver::Q.contains(s)) {
				QHash<State, float>& q = QLearningSolver::Q[s];
				foundState = optimalValue(mc, &sNew, q, closeList); // Chose action
			}
		}
		++nbChoices;


		if (sNew != s && ! closeList.contains(sNew) && foundState != -std::numeric_limits<float>::max()) {
			/* Action is selected */

			// closeList.insert(s); Removed close list

			if ( QLearningSolver::Q.contains(s) ) {
				QHash<State, float>& q = QLearningSolver::Q[s];
				if ( ! q.contains(sNew) ) {
					q.insert(sNew, 0.0f);
				}

				updateStateActionValue(mc, s, sNew, q, ALPHA, GAMMA, closeList);
			} else {
				QHash<State, float> q;
				q.insert(sNew, 0.0f);
				updateStateActionValue(mc, s, sNew, q, ALPHA, GAMMA, closeList);
				QLearningSolver::Q.insert(s, q);
			}

			mvt++;
			s = sNew;
		}
	} while ( !mc.isAccomplished(s) && mRun.load() && nbChoices < MAX_NB_CHOICES);

    //std::cout << "######### - FINISHED en " << mvt << "mouvements" << std::endl;
}

void QLearningSolver::updateStateActionValue(MissionContext const& mc, State s, State sNew, QHash<State, float>& q, float a, float g, QSet<State> const& c) {

	float futureOptimalValue = optimalValue(mc, QLearningSolver::Q.value(sNew));
	float val = 0;
	float reward = generateReward(mc, s, sNew, c);

	val = (1 - a) * q.value(sNew) + a * (reward + g * futureOptimalValue);

	q.insert(sNew, val);
}

QVector<State> QLearningSolver::buildSolution(MissionContext const& mc) {

	State s = mc.getInitState();
	State sNew = mc.getInitState();
	int mvt = 0;
	int nbLoops = 0;
	QHash<State, float> q;

	QSet<State> chosen;

	QVector<State> solution;

	//std::cout << "On commence à build la solution" << std::endl;


	//std::cout << "Total discovered states" << QLearningSolver::Q.size();
	while ( !mc.isAccomplished(s) && nbLoops < 6 ) {
		mvt = 0;
		chosen.clear();
		solution.clear();
		s = mc.getInitState();
		sNew = mc.getInitState();

		while ( !mc.isAccomplished(s) && mvt < 50000) {
			chosen.insert(s);
			solution.append(s);
			q = QLearningSolver::Q.value(s);
			if (optimalValue(mc, &sNew, q, chosen, nbLoops * 20) == -std::numeric_limits<float>::max()) {
				//std::cout << "It is looping" << std::endl;
				mvt = 50000;
			}
			s = sNew;

			++mvt;
		}
		++nbLoops;
	}

	//std::cout << "The solution found is " << mvt << std::endl;

	solution.append(s); // Put final state

	//std::cout << "Résolu en " << mvt << "mouvements par le QLearning" << std::endl;

	return solution;
}

QString QLearningSolver::getSolverName()
{
    return "Q-Learning";
}

float QLearningSolver::optimalValue(MissionContext const& mc, QHash<State, float> const& q) {
	QSet<State> c;
    return optimalValue(mc, nullptr, q, c);
}

float QLearningSolver::optimalValue(MissionContext const& mc, State* s, QHash<State, float> const& q, QSet<State>& c) {
	return optimalValue(mc, s, q, c, -1);
}

float QLearningSolver::optimalValue(MissionContext const& mc, State* s, QHash<State, float> const& q, QSet<State>& c, int randomRate) {
	Q_UNUSED(mc);

	QHash<State, float>::const_iterator it = q.constBegin();

	float optimalValue = -std::numeric_limits<float>::max();

	int found = 0;

	while (it != q.constEnd()) {
		if (!c.isEmpty()) {
			/* Case close list */
			if (found == 1) {
				/* Randomize exploration with a randomRate factor. Not so useful */
				if (it.value() >= optimalValue && !c.contains(it.key()) && rand() % 100 > randomRate) {
					found = 1;
					optimalValue = it.value();
					if (s != nullptr) {
						*s = q.key(optimalValue);
					}
				}
			} else {
				if (it.value() > optimalValue && !c.contains(it.key())) {
					found = 1;
					optimalValue = it.value();
					if (s != nullptr) {
						*s = q.key(optimalValue);
					}
				}
			}
		} else {
			if (it.value() > optimalValue ) {
				found = 1;
				optimalValue = it.value();
				if (s != nullptr) {
					*s = q.key(optimalValue);
				}
			}
		}

		++it;
	}

	if (optimalValue == -std::numeric_limits<float>::max()) {
		if (!c.isEmpty()) {
			return -std::numeric_limits<float>::max();
		} else {
			return 0;
		}
	}

	return optimalValue;
}

float QLearningSolver::generateReward(MissionContext const& mc, State const& sOld, State const& sNew, QSet<State> const& c) {
	Q_UNUSED(c);
	if (mc.isAccomplished(sNew)) {
		/* Final state */
		return 10.0f;
	}
	if (sOld == sNew) {
		/* Same state */
		return -10.0f;
	}

	/* Default state */
	return -0.0f;
}


bool QLearningSolver::isRunning() {
	return nbRequestedEpisodes == currentEpisode ? true : false;
}
