#ifndef SOLVER_H
#define SOLVER_H

#include <exception>
#include <QObject>
#include <QVector>
#include <QTimer>
#include <QThread>
#include <QDebug>
#include <QEventLoop>

#include "state.h"
#include "missioncontext.h"

namespace Game {
class Solver : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int solutionLength READ getSolutionLength NOTIFY think)

public:
    enum class DifficultyLevel : quint8 {
        BEGINNER = 0,
        MEDIUM,
        CHALLENGING,
        HARD,
        INSANE
    };
    Q_ENUM(DifficultyLevel)

    enum class Algorithm {
        A_STAR = 0,
        Q_LEARNING
    };
    Q_ENUM(Algorithm)


    Solver(QObject *parent = nullptr);

    virtual ~Solver();

    virtual QString getSolverName();

    void solve(MissionContext const& mc);

    void interrupt();

    void wait();

    QVector<DifficultyLevel> getAvailableDifficultyLevel() const;

    int getMaxThinkDelay() const;
    void setMaxThinkDelay(int const& getMaxThinkDelay);

    int getRemainingThinkTime() const;

    DifficultyLevel getSelectedDifficultyLevel() const;
    void setSelectedDifficultyLevel(DifficultyLevel const& difficultyLevel);

    QVector<State> getSolution() const;

    int getSolutionLength() const;

    bool isSolutionFound() const;

    bool isSolving() const;

signals:
    void finished(bool isTimeOut);
    void think(QVector<Game::State> solution);

protected:
    virtual void syncSolve(MissionContext const& mc, DifficultyLevel difficultyLevel) = 0;
    void setAvailableDifficultyLevels(QVector<DifficultyLevel> const& availableDifficultyLevels);

    std::atomic_bool mRun;

private slots:
    void sendSolutionSlot(QVector<Game::State> solution);
    void timeoutSlot();

private:
    void stopThread();
    void stopTimer();

    QThread *mThread;
    QTimer *mTimer;
    QVector<DifficultyLevel> mAvailableDifficultyLevels;
    QVector<State> mSolution;
    int mMaxThinkDelay;
    DifficultyLevel mSelectedDifficultyLevel;

signals:
    void sendSolution(QVector<Game::State> solution);
};
}

Q_DECLARE_METATYPE(Game::Solver const*)
Q_DECLARE_METATYPE(Game::Solver::DifficultyLevel)
Q_DECLARE_METATYPE(Game::Solver::Algorithm)
Q_DECLARE_METATYPE(QVector<Game::State>)

#endif // SOLVER_H
