#include "solverstats.h"

using namespace Game;

SolverStats::SolverStats(Solver &solver) : mSolver(solver)
{
    connect(&mSolver, SIGNAL(finished(bool)), this, SLOT(onFinished(bool)));
    connect(&mSolver, SIGNAL(think(QVector<Game::State>)), this, SLOT(onThink(QVector<Game::State>)));
}

void SolverStats::startSimulation()
{
    startSimulation(mPoolTest);
}

void SolverStats::startSimulation(int nbSimulation)
{
    randomPoolTest(nbSimulation);
    startSimulation();
}

void SolverStats::startSimulation(const QVector<MissionContext> &poolTest)
{
    mMissionIndex = -1;
    std::cout << "Start pool" << std::endl;
    for (QVector<MissionContext>::const_iterator it = poolTest.begin(); it != poolTest.end(); ++it) {
        mCurrentMissionContext = *it;
        mBestSolution = QVector<State>();
        ++mMissionIndex;
        mTime.start();
        mSolver.solve(*it);
        mSolver.wait();
        std::cout << "Test number : " << mMissionIndex << " (terminated)" << std::endl;
    }
    std::cout << "Pool finished" << std::endl;
}

void SolverStats::randomPoolTest(int poolSize)
{
    mPoolTest = QVector<MissionContext>(poolSize);
    mBoards = QVector<Board>(poolSize);
    RandomBoardGenerator randomizer;
    MissionContext mission;

    for (int i = 0; i < poolSize; ++i) {
        Board b;
        randomizer.randomize(b);
        mBoards[i] = b;
        mPoolTest[i] = mission.createRandom(mBoards[i]);
    }
}

QVector<Stat> SolverStats::stats() const
{
    return mStats;
}

void SolverStats::onFinished(bool isTimeOut)
{
    Stat stat;
    stat.missionIndex = mMissionIndex;
    stat.solution = mBestSolution.size();
    stat.maxMsTime = mSolver.getMaxThinkDelay();
    stat.difficulty = mSolver.getSelectedDifficultyLevel();
    stat.solverName = mSolver.getSolverName();
    stat.initState = mCurrentMissionContext.getInitState();
    stat.target = mCurrentMissionContext.getTarget();

    if (isTimeOut) {
        stat.solution = 255;
        stat.msTime = mSolver.getMaxThinkDelay();
        stat.interrupted = true;
    } else {
        stat.msTime = mTime.elapsed();
        stat.interrupted = false;
    }

    mStats.append(stat);
}

void SolverStats::onThink(QVector<Game::State> solution)
{
    if (mBestSolution.isEmpty() || (mBestSolution.size() > solution.size() && solution.size()))
        mBestSolution = solution;
}

QVector<Board> SolverStats::boards() const
{
    return mBoards;
}

void SolverStats::setBoards(const QVector<Board> &boards)
{
    mBoards = boards;
}

QVector<MissionContext> SolverStats::poolTest() const
{
    return mPoolTest;
}

void SolverStats::setPoolTest(const QVector<MissionContext> &poolTest)
{
    mPoolTest = poolTest;
}

namespace Game {
    std::ostream& operator<<(std::ostream &o, const SolverStats &st)
    {
        QVector<Stat> stats = st.stats();
        const std::string separator = ",";

        o <<
             "SOLUTION" << separator <<
             "MISSION_INDEX" << separator <<
             "ROBOT_RED_X" << separator <<
             "ROBOT_RED_Y" << separator <<
             "ROBOT_BLUE_X" << separator <<
             "ROBOT_BLUE_Y" << separator <<
             "ROBOT_YELLOW_X" << separator <<
             "ROBOT_YELLOW_Y" << separator <<
             "ROBOT_GREEN_X" << separator <<
             "ROBOT_GREEN_Y" << separator <<
             "TARGET_COLOR" << separator <<
             "TARGET_X" << separator <<
             "TARGET_Y" << separator <<
             "MS_TIME" << separator <<
             "INTERRUPTED" << separator <<
             "MAX_MS_TIME" << separator <<
             "DIFFICULTY" << separator <<
             "SOLVER_NAME";

        for (QVector<Stat>::const_iterator it = stats.begin(); it != stats.end(); ++it) {
            o << std::endl;
            o <<
                 it->solution << separator <<
                 it->missionIndex << separator <<
                 it->initState.getRobotPos(Robot(Color::RED)).getX() << separator <<
                 it->initState.getRobotPos(Robot(Color::RED)).getY() << separator <<
                 it->initState.getRobotPos(Robot(Color::BLUE)).getX() << separator <<
                 it->initState.getRobotPos(Robot(Color::BLUE)).getY() << separator <<
                 it->initState.getRobotPos(Robot(Color::YELLOW)).getX() << separator <<
                 it->initState.getRobotPos(Robot(Color::YELLOW)).getY() << separator <<
                 it->initState.getRobotPos(Robot(Color::GREEN)).getX() << separator <<
                 it->initState.getRobotPos(Robot(Color::GREEN)).getY() << separator <<
                 static_cast<int>(it->target.getColor()) << separator <<
                 it->target.getPos().getX() << separator <<
                 it->target.getPos().getY() << separator <<
                 it->msTime << separator <<
                 it->interrupted << separator <<
                 it->maxMsTime << separator <<
                 static_cast<int>(it->difficulty ) << separator <<
                 it->solverName.toStdString();
        }

        return o;
    }
}
