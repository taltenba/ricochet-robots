#include <board.h>

using namespace Game;

Board::Board() : mSquares(), mValidTargetPos()
{
    // Empty
}

BoardSquare const& Board::getSquare(BoardPos const& pos) const
{
    return mSquares[pos.getX()][pos.getY()];
}

QVariant Board::getWrappedSquare(int i, int j) const
{
    return QVariant::fromValue(mSquares[i][j]);
}

BoardSquare const (*Board::getAllSquares() const)[HEIGHT]
{
    return mSquares;
}

BoardPos Board::getRandomPos(QSet<BoardPos> const& excludedPos) const
{
    BoardPos pos;

    do {
        pos = BoardPos(rand() % WIDTH, rand() % HEIGHT);
    } while (excludedPos.contains(pos));

    return pos;
}

BoardPos Board::getRandomTargetPos(QSet<BoardPos> const& excludedPos) const
{
    Q_ASSERT(mValidTargetPos.size() > 0);

    BoardPos pos;

    do {
        pos = mValidTargetPos[rand() % mValidTargetPos.size()];
    } while (excludedPos.contains(pos));

    return pos;
}

BoardPos Board::getPrecomputedMoveResult(BoardPos pos, Move::Direction moveDir) const
{
    return mMoveResults[pos.getX()][pos.getY()][static_cast<int>(moveDir)];
}

void Board::setValidTargetPos(QVector<BoardPos> const& pos)
{
    mValidTargetPos = pos;
}

BoardSquare const* Board::operator[](int x) const
{
    return mSquares[x];
}

BoardSquare* Board::operator[](int x)
{
    return mSquares[x];
}

void Board::precomputeMoves()
{
    for (int i = 0; i < Board::WIDTH; ++i)
    {
        for (int j = 0; j < Board::HEIGHT; ++j)
        {
            for (int k = 0; k < Move::DIRECTION_COUNT; ++k)
            {
                Move::Direction dir = static_cast<Move::Direction>(k);
                QPoint vector = Move::getVector(dir);
                BoardSquare::Wall blockingWall = Move::getBlockingWall(dir);

                BoardPos result = BoardPos(i, j);

                while (!getSquare(result).hasWall(blockingWall))
                    result = result + vector;

                mMoveResults[i][j][k] = result;
            }
        }
    }
}
