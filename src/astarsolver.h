#ifndef ASTARSOLVER_H
#define ASTARSOLVER_H

#include <functional>
#include <cmath>
#include <QVector>
#include <QHash>
#include <QSet>
#include <QLinkedList>
#include <iostream>

#include "solver.h"
#include "board.h"
#include "state.h"
#include "target.h"
#include "robot.h"

namespace Game {

/**
 * @brief The AStarSolver class : uses the A* algorithm to solve the Ricochet Robots problem
 */
class AStarSolver : public Solver
{

#define HEURISTIC(h) std::bind(&AStarSolver::h, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)

public:
    static const QVector<DifficultyLevel> AVAILABLE_DIFFICULTIES;

    /*  Constructor and destructor  */
    AStarSolver(QObject *parent = nullptr);
    ~AStarSolver();

    QString getSolverName();

protected:

    /**
     * @brief syncSolve : synchronous solver
     * @param mc : the mission context to solve, contains a board,
     *             an initial state and a target
     * @param difficultyLevel : the difficulty level to be reached by the AI
     */
    void syncSolve(MissionContext const& mc, DifficultyLevel difficultyLevel);

private:

    /*  Node structure : the parent's state and the actual valuation of g(n) ( with f(n) = g(n) + h(n) )    */
    struct Node {
        State parent;
        quint8 gScore;
    };

    /*  ComparableState structure, used for the openSet, the openSet is sorted on f(n)  */
    struct ComparableState {
        State state;
        quint8 fScore;
    };

    /*  Redefinition of comparaison operator    */
    friend bool operator<(AStarSolver::ComparableState s1, AStarSolver::ComparableState s2);

    /**
     * @brief aStarAlgorithm : implements the A* algorithm to find a solution according to the given mission context,
     *                         the solution can be worsen by the given heuristic
     * @param mc : the mission context (board, target, initial state)
     * @param heuristic : the heuristic to use
     */
    void aStarAlgorithm(
            MissionContext const& mc,
            std::function<quint8(Board const&, State, Target const&, Robot const&)> heuristic
            );

    /**
     * @brief isTargetReached : returns true if the target is reached
     * @param current : current state (contains robot positions)
     * @param target : target (contains the target's color and position)
     * @return true if the target is reached
     */
    bool isTargetReached(State current, Target const& target);

    /**
     * @brief reconstructPath : reconstructs the solution in order to give a detailled solution to the user
     * @param cameFrom : the map of nodes that contains the actual graph
     * @param current : the current state
     * @param start : the starting state
     * @return a vector that contains all state (moves) to do in order to reach the target
     */
    QVector<State> reconstructPath(QHash<State, Node> const& cameFrom, State const& current, State start);

    /**
     * @brief estimate : estimates the "distance" that separate a moving robot to the target
     * @param mc : the mission context (with the board, target and initial state)
     * @param state : the current state
     * @param movingRobot : the robot to move
     * @param forceAll : if true, considers that all robot are moving
     * @return the least estimated value
     */
    quint8 estimate(MissionContext const& mc, State state, Robot const& movingRobot, bool forceAll = false);

    /**
     * @brief initEstimatedMoveTab : initiates the estimated board of move, this "board" is used by the humman heuristic
     */
    void initEstimatedMoveTab();

    /**
     * @brief getEstimatedValueFormPos : returns the actual estimated number of move according to a given position
     * @param pos : position to evaluate
     * @return the actual estimated number of move according to a given position
     */
    quint8 getEstimatedValueFormPos(const BoardPos &pos);

    /**
     * @brief setEstimatedValueFormPos : sets the new estimated value (for the number of moves) according to a position
     * @param pos : position to set
     * @param newVal : new estimated value
     */
    void setEstimatedValueFormPos(BoardPos const& pos, quint8 newVal);

    /**
     * @brief displayEstimatedBoard : debug function, prints the evaluated moves for the current state and mission context
     * @param o : an ostream
     */
    void displayEstimatedBoard(std::ostream &o);

    /**
     * @brief humanHeuristic : admissible heuristique to compute a minimum value for the remaining number of moves to reach the target
     * @param board : actual board
     * @param state : current state
     * @param target : target to achieve
     * @param movingRobot : robot to move
     * @return an estimated value of remaining moves, the real value is higher or same
     */
    quint8 humanHeuristic(Board const& board, State state, Target const& target, Robot const& movingRobot);

    /**
     * @brief abs : computes the absolute value
     * @param a : an integer
     * @return the absolute value of a
     */
    int abs(int a);

    /**
     * @brief manhattanHeuristic : computes the manhattan distance between the moving robot and the target
     * @param board : actual board
     * @param state : current state
     * @param target : target to achieve
     * @param movingRobot : robot to move
     * @return the manhattan distance between the moving robot and the target
     */
    quint8 manhattanHeuristic(Board const& board, State state, Target const& target, Robot const& movingRobot);

    /**
     * @brief euclidianHeuristic : computes the euclidian distance between the moving robot and the target
     * @param board : actual board
     * @param state : current state
     * @param target : target to achieve
     * @param movingRobot : robot to move
     * @return the euclidian distance between the moving robot and the target
     */
    quint8 euclidianHeuristic(Board const& board, State state, Target const& target, Robot const& movingRobot);

    /**
     * @brief mediumHeuristic : computes the distance according to the humanHeuristic apply randomly a worsing value
     * @param board : actual board
     * @param state : current state
     * @param target : target to achieve
     * @param movingRobot : robot to move
     * @return the worsed value
     */
    quint8 mediumHeuristic(Board const& board, State state, Target const& target, Robot const& movingRobot);

    /**
     * @brief dummyHeuristic : computes a full random value
     * @param board : actual board
     * @param state : current state
     * @param target : target to achieve
     * @param movingRobot : robot to move
     * @return a full random value
     */
    quint8 dummyHeuristic(Board const& board, State state, Target const& target, Robot const& movingRobot);


    /*  Private attributes  */
    std::function<quint8(Board const&, State, Target const&, Robot const&)> mHeuristic;
    QLinkedList<BoardPos> *mStartCells;
    quint8 mEstimatedMoveBoard[Board::HEIGHT][Board::WIDTH];
};

}

#endif // ASTARSOLVER_H
