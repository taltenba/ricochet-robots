#ifndef MISSION_H
#define MISSION_H

#include <QObject>
#include <QTimer>
#include <missioncontext.h>

namespace Game {
    /**
     * @brief Represents a game mission.
     */
    class Mission : public QObject
    {
        Q_OBJECT

        Q_PROPERTY(Game::MissionContext context READ getContext CONSTANT)
        Q_PROPERTY(int duration READ getDuration CONSTANT)
        Q_PROPERTY(int remainingTime READ getRemainingTime NOTIFY remainingTimeChanged)

    public:
        Mission(MissionContext ctx, int duration, QObject *parent = nullptr);

        void start();

        Q_INVOKABLE
        void complete();

        Q_INVOKABLE
        void abort();

        MissionContext const& getContext() const;

        int getRemainingTime() const;

        int getDuration() const;

    signals:
        void missionStarted();

        void missionOver(bool wasAborted);

        void remainingTimeChanged(int remainingMillis);

    private slots:
        void timeTick();

    private:
        MissionContext mContext;
        QTimer* mTimer;
        int mRemainingTime;
        int mDuration;

        void terminate(bool wasAborted);
    };
}

#endif // MISSION_H
