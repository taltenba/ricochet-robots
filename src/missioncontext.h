#ifndef MISSIONCONTEXT_H
#define MISSIONCONTEXT_H

#include <QObject>
#include <state.h>
#include <board.h>
#include <target.h>

namespace Game {
    /**
     * @brief Represents the context of a game mission, that is to say the game board,
     *        the initial game state and the target to reach.
     */
    class MissionContext
    {
        Q_GADGET

        Q_PROPERTY(Game::Board board READ getBoard CONSTANT)
        Q_PROPERTY(Game::State initState READ getInitState CONSTANT)
        Q_PROPERTY(Game::Target target READ getTarget CONSTANT)

    public:
        MissionContext();
        MissionContext(Board const& board, State initState, Target target);

        Board const& getBoard() const;

        Target const& getTarget() const;

        State getInitState() const;

        bool isAccomplished(State state) const;

        bool isTrivial() const;

        static MissionContext createRandom(Board const& board);

    private:
        Board const* mBoard;
        State mInitState;
        Target mTarget;

        bool canReachTargetInOneMove(Robot robot) const;
    };
}

Q_DECLARE_METATYPE(Game::MissionContext)

#endif // MISSIONCONTEXT_H
