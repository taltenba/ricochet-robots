#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>
#include <QTimer>
#include <board.h>
#include <mission.h>
#include <player.h>
#include <score.h>
#include <randomboardgenerator.h>
#include <solver.h>

namespace Game {
    /**
     * @brief The game engine.
     */
    class Engine : public QObject
    {
        Q_OBJECT

        Q_PROPERTY(Game::Board board READ getBoard NOTIFY newGameStarted)
        Q_PROPERTY(int missionCount READ getMissionCount NOTIFY newGameStarted)
        Q_PROPERTY(int missionDuration READ getMissionDuration NOTIFY newGameStarted)
        Q_PROPERTY(Game::Solver const* ai READ getAi NOTIFY newGameStarted)
        Q_PROPERTY(int finishedMissionCount READ getFinishedMissionCount NOTIFY missionOver)
        Q_PROPERTY(Game::Player* userPlayer READ getUserPlayer NOTIFY newMissionStarted)
        Q_PROPERTY(Game::Mission* currentMission READ getCurrentMission NOTIFY newMissionStarted)
        Q_PROPERTY(Game::Score score READ getScore CONSTANT)

    public:
        enum class MissionResult
        {
            COMPUTER_WINS = 0,
            USER_WINS,
            DRAW
        };
        Q_ENUM(MissionResult)

        explicit Engine(QObject *parent = nullptr);

        Q_INVOKABLE
        void newGame(int missionCount, int missionDuration, Game::Solver::Algorithm ai, Game::Solver::DifficultyLevel difficulty);

        Q_INVOKABLE
        bool nextMission();

        Q_INVOKABLE
        void completeMission();

        Q_INVOKABLE
        void stop();

        int getMissionCount() const;

        int getMissionDuration() const;

        int getFinishedMissionCount() const;

        Board const& getBoard() const;

        Mission* getCurrentMission() const;

        Player* getUserPlayer() const;

        Solver const* getAi() const;

        Score const& getScore() const;

    signals:
        void newGameStarted();

        void newMissionStarted();

        void missionOver(QVariantList aiSolutionStates, Game::Engine::MissionResult missionResult);

        void gameOver();

    private slots:
        void onMissionOver(bool wasAborted);

    private:
        Board mBoard;
        RandomBoardGenerator mBoardGenerator;
        Mission* mCurrentMission;
        Player* mUserPlayer;
        Solver* mAiSolver;
        Score mScore;
        int mMissionCount;
        int mMissionDuration;
        int mFinishedMissionCount;

        //QVariantList buildAISolutionStates(QVector<Move> const& aiSolution) const;
        QVariantList wrapSolution(QVector<State> const& solution) const;
    };
}

Q_DECLARE_METATYPE(Game::Engine::MissionResult)

#endif // ENGINE_H
