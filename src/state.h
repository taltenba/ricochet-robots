#ifndef STATE_H
#define STATE_H

#include <QObject>
#include <QVariant>
#include <QHash>
#include <iostream>
#include <color.h>
#include <move.h>
#include <board.h>
#include <robot.h>

namespace Game {
    /**
     * @brief Represents a game state, that is to say the position of each robot on the board.
     *
     * Note that an instance of this class is immutable.
     */
    class State
    {
        Q_GADGET

    public:
        /**
         * @brief Creates a new zero-initialized state
         */
        State();

        /**
         * @brief Creates a state from specified robot positions
         * @param robotPos The robot positions
         */
        State(BoardPos robotPos[Robot::ROBOT_COUNT]);

        /**
         * @brief Indicates whether or not a specified move can be made (ie. changes
         *        the state of the game)
         * @param board The game board
         * @param move The move
         * @return true if the specified move can be made, false otherwise
         */
        bool canRobotMove(Board const& board, Move move) const;

        /**
         * @brief Moves a robot at the specified position
         * @param board The game board
         * @param move The move
         * @return the new state
         */
        State moveRobot(Board const& board, Move move) const;

        /**
         * @brief Returns the position of the specified robot
         * @param robot The robot
         * @return the position of the specified robot
         */
        Q_INVOKABLE
        Game::BoardPos getRobotPos(Game::Robot robot) const;

        /**
         * @brief Sets the position of the specified robot
         * @param robot The robot
         * @param pos The new robot position
         * @return the new state
         */
        State setRobotPos(Robot robot, BoardPos const& pos) const;

        Robot const* findRobot(BoardPos pos) const;

        /**
         * @brief Finds the robot at the specified position, if any, and
         *        wraps the result in a QVariant
         * @param x The x-coordinate
         * @param y The y-coordinate
         * @return the robot at the specified position, if any, wrapped in
         *         a QVariant, an empty QVariant otherwise
         */
        Q_INVOKABLE
        QVariant findWrappedRobot(int x, int y) const;

        bool operator==(State other);
        bool operator!=(State other);

        /**
         * @brief Returns the hash of this state
         * @return the hash of this state
         */
        quint32 getHash() const;

        friend std::ostream& operator<<(std::ostream& os, State state);

    private:
        BoardPos mRobotPos[Robot::ROBOT_COUNT];
    };

    inline bool operator==(const Game::State &e1, const Game::State &e2) {
        State e = e1;
        return e.operator==(e2);
    }

    inline uint qHash(const Game::State &key) {
        return key.getHash();
    }
}

Q_DECLARE_METATYPE(Game::State)

#endif // STATE_H
