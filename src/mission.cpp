#include "mission.h"

using namespace Game;

#define TIMER_GRANULARITY_MILLIS 1000

Mission::Mission(MissionContext ctx, int duration, QObject *parent)
    : QObject(parent), mContext(ctx), mTimer(), mRemainingTime(-1), mDuration(duration)
{
    Q_ASSERT(duration >= 0);

    mTimer = new QTimer(this);
    connect(mTimer, SIGNAL(timeout()), this, SLOT(timeTick()));
}

void Mission::start()
{
    mRemainingTime = mDuration;
    mTimer->start(TIMER_GRANULARITY_MILLIS);

    emit remainingTimeChanged(mRemainingTime);
    emit missionStarted();
}

void Mission::complete()
{
    if (mRemainingTime == -1)
        return;

    terminate(false);
}

void Mission::abort()
{
    if (mRemainingTime == -1)
        return;

    terminate(true);
}

MissionContext const& Mission::getContext() const
{
    return mContext;
}

int Mission::getRemainingTime() const
{
    return mRemainingTime;
}

int Mission::getDuration() const
{
    return mDuration;
}

void Mission::timeTick()
{
    if (mRemainingTime <= TIMER_GRANULARITY_MILLIS)
        mRemainingTime = 0;
    else
        mRemainingTime -= TIMER_GRANULARITY_MILLIS;

    emit remainingTimeChanged(mRemainingTime);

    if (mRemainingTime == 0)
        terminate(false);
}

void Mission::terminate(bool wasAborted)
{
    mTimer->stop();
    mRemainingTime = -1;
    emit missionOver(wasAborted);
}
