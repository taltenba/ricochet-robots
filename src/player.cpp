#include "player.h"

using namespace Game;

Player::Player(MissionContext const& mission, QObject* parent)
    : QObject(parent), mMission(mission), mMoveHistory(),
      mBestSolution(), mGameState(mission.getInitState())
{
    mMoveHistory.push(mGameState);
}

void Player::play(Move const& move)
{
    State newState = mGameState.moveRobot(mMission.getBoard(), move);

    if (newState == mGameState)
        return;

    mGameState = newState;

    mMoveHistory.push(newState);

    if (mMission.isAccomplished(mGameState))
    {
        if (mBestSolution.isEmpty() || mMoveHistory.size() < mBestSolution.size())
        {
            mBestSolution = mMoveHistory;
            emit bestSolutionChanged(mBestSolution.size() - 1);
        }

        mMoveHistory.clear();
        mGameState = mMission.getInitState();
        mMoveHistory.push(mGameState);
    }

    emit gameStateChanged(mGameState, mMoveHistory.size() - 1);
}

void Player::play(Robot robot, Move::Direction moveDir)
{
    play(Move(robot, moveDir));
}

void Player::undoLastMove()
{
    Q_ASSERT(mMoveHistory.size() > 1);

    mMoveHistory.pop();
    mGameState = mMoveHistory.top();
    emit gameStateChanged(mGameState, mMoveHistory.size() - 1);
}

void Player::undoAllMoves()
{
    mMoveHistory.clear();

    mGameState = mMission.getInitState();
    mMoveHistory.push(mGameState);
    emit gameStateChanged(mGameState, 0);
}

bool Player::canUndoMove() const
{
    return mMoveHistory.size() > 1;
}

bool Player::hasFoundSolution() const
{
    return mBestSolution.size() > 0;
}

MissionContext const& Player::getMissionContext() const
{
    return mMission;
}

State Player::getGameState() const
{
    return mGameState;
}

QVector<State> Player::getBestSolution() const
{
    return mBestSolution;
}

int Player::getBestSolutionLength() const
{
    return mBestSolution.size() - 1;
}

int Player::getMoveCount() const
{
    return mMoveHistory.size() - 1;
}
