#ifndef ROBOT_H
#define ROBOT_H

#include <QObject>
#include <color.h>

namespace Game {
    /**
     * @brief Represents a robot
     */
    class Robot {
        Q_GADGET

        Q_PROPERTY(Game::Color color READ getColor CONSTANT)

    public:
        static constexpr int ROBOT_COUNT = 4;
        static const Robot ROBOTS[ROBOT_COUNT];

        Robot();
        explicit Robot(Color color);

        Color getColor() const;

    private:
        Color mColor;
    };
}

Q_DECLARE_METATYPE(Game::Robot)

#endif // ROBOT_H
