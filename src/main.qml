import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Shapes 1.12
import com.arp.ricochetrobots.GameBoard 1.0
import com.arp.ricochetrobots.BoardSquare 1.0
import com.arp.ricochetrobots.BoardPos 1.0
import com.arp.ricochetrobots.Target 1.0
import com.arp.ricochetrobots.Mission 1.0
import com.arp.ricochetrobots.MissionContext 1.0
import com.arp.ricochetrobots.Move 1.0
import com.arp.ricochetrobots.Player 1.0
import com.arp.ricochetrobots.Robot 1.0
import com.arp.ricochetrobots.Score 1.0
import com.arp.ricochetrobots.State 1.0
import com.arp.ricochetrobots.GameColor 1.0
import com.arp.ricochetrobots.GameEngine 1.0
import com.arp.ricochetrobots.Solver 1.0

ApplicationWindow {
    visible: true
    width: 1200
    height: 800
    minimumWidth: 1200
    minimumHeight: 800
    title: "Richochet Robots"

    GameEngine {
        id: engine

        onMissionOver: missionOverDialog.openDialog(engine.userPlayer.bestSolutionLength,
                                                    aiSolutionStates, engine.currentMission.context,
                                                    missionResult);
    }

    onClosing: engine.stop()

    MissionOverDialog {
        id: missionOverDialog
        parent: ApplicationWindow.overlay
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        focus: true

        onClosed: {
            if (!engine.nextMission())
                gameOverDialog.openDialog(engine.score);

            gameView.forceActiveFocus();
        }
    }

    GameOverDialog {
        id: gameOverDialog
        parent: ApplicationWindow.overlay
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        focus: true

        onClosed: {
            newGameDialog.open();
        }
    }

    NewGameDialog {
        id: newGameDialog
        parent: ApplicationWindow.overlay
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        visible: true
        closePolicy: Popup.CloseOnEscape
        focus: true

        onAccepted: engine.newGame(missionCount, missionDuration * 1000, aiAlgorithm, aiStrength)
    }

    WaitAiSolutionDialog {
        id: waitAiSolutionDialog
        parent: ApplicationWindow.overlay
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        focus: true

        onAccepted: engine.currentMission.complete()
        onClosed: gameView.forceActiveFocus()
    }

    /*Dialog {
        id: restartGameConfirmationDialog
        title: "Are you sure?"
        standardButtons: Dialog.Yes | Dialog.No

        Label {
            text: "Do you really want to start a new game?"
        }

        onAccepted: {
            engine.currentMission.abort();
            newGameDialog.open();
        }
    }*/

    GridLayout {
        anchors.fill: parent
        anchors.margins: 20
        columnSpacing: 20

        Keys.onPressed: {
            if (!(event.modifiers & Qt.ControlModifier))
                return;

            switch (event.key) {
                case Qt.Key_C:
                    if (engine.ai.solutionLength === 0)
                        waitAiSolutionDialog.openDialog(engine.ai);
                    else
                        engine.currentMission.complete();

                    break;
                case Qt.Key_N:
                    engine.currentMission.abort();
                    newGameDialog.open();
                    break;
            }
        }

        Column {
            id: infoColumn
            spacing: 20
            Layout.row: 0
            Layout.column: 1
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignTop

            Label {
                id: missionLabel
                anchors.left: parent.left
                anchors.right: parent.right
                text: "Mission #1"
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 0.07 * parent.height
                font.bold: true

                Connections {
                    target: engine
                    onNewMissionStarted: missionLabel.text = "Mission #" + (engine.finishedMissionCount + 1)
                }
            }

            Rectangle {
                anchors.left: parent.left
                anchors.right: parent.right
                height: 0.252 * parent.height
                border.width: 0.0035 * parent.height
                border.color: Material.foreground
                color: "transparent"

                Column {
                    anchors.fill: parent
                    anchors.margins: 0.0175 * infoColumn.height
                    spacing: 0.0125 * infoColumn.height

                    Label {
                        anchors.left: parent.left
                        anchors.right: parent.right
                        text: qsTr("Score")
                        horizontalAlignment: Text.AlignHCenter
                        font.bold: true
                        font.pixelSize: 0.05 * infoColumn.height
                    }

                    Row {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        Label {
                            width: parent.width / 2
                            text: qsTr("You")
                            horizontalAlignment: Text.AlignHCenter
                            font.bold: true
                            font.pixelSize: 0.035 * infoColumn.height
                            Layout.row: 1
                            Layout.column: 0
                        }

                        Label {
                            width: parent.width / 2
                            text: qsTr("AI")
                            horizontalAlignment: Text.AlignHCenter
                            font.bold: true
                            font.pixelSize: 0.035 * infoColumn.height
                            Layout.row: 1
                            Layout.column: 1
                        }
                    }

                    Row {
                        anchors.left: parent.left
                        anchors.right: parent.right

                        Label {
                            id: userScoreLabel
                            width: parent.width / 2
                            text: "0"
                            horizontalAlignment: Text.AlignHCenter
                            font.pixelSize: 0.07 * infoColumn.height
                            Layout.row: 2
                            Layout.column: 0
                            Layout.fillWidth: true
                        }

                        Label {
                            id: aiScoreLabel
                            width: parent.width / 2
                            text: "0"
                            horizontalAlignment: Text.AlignHCenter
                            font.pixelSize: 0.07 * infoColumn.height
                            Layout.row: 2
                            Layout.column: 1
                            Layout.fillWidth: true
                        }

                        Connections {
                            target: engine

                            onNewGameStarted: {
                                userScoreLabel.text = "0";
                                aiScoreLabel.text = "0";
                            }

                            onMissionOver: {
                                var score = engine.score;

                                userScoreLabel.text = score.getPoints(Score.USER);
                                aiScoreLabel.text = score.getPoints(Score.COMPUTER);
                            }
                        }
                    }
                }
            }

            Rectangle {
                anchors.left: parent.left
                anchors.right: parent.right
                height: 0.252 * parent.height
                border.width: 0.0035 * parent.height
                border.color: Material.foreground
                color: "transparent"

                Column {
                    anchors.fill: parent
                    anchors.margins: 0.0175 * infoColumn.height
                    spacing: 0.0125 * infoColumn.height

                    Label {
                        anchors.left: parent.left
                        anchors.right: parent.right
                        text: qsTr("Remaining time")
                        horizontalAlignment: Text.AlignHCenter
                        font.bold: true
                        font.pixelSize: 0.05 * infoColumn.height
                    }

                    Label {
                        property Mission mission: null;

                        id: remainingTimeLabel
                        anchors.left: parent.left
                        anchors.right: parent.right
                        text: "00:00"
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 0.125 * infoColumn.height

                        function updateTime(remainingMillis) {
                            var remainingSecs = remainingMillis / 1000;
                            var minutes = Math.floor(remainingSecs / 60);
                            var seconds = remainingSecs % 60;

                            var minuteStr = ("00" + minutes).slice(-2);
                            var secondStr = ("00" + seconds).slice(-2);

                            remainingTimeLabel.text = minuteStr + ":" + secondStr;
                        }

                        Connections {
                            target: engine
                            onNewMissionStarted: {
                                remainingTimeLabel.mission = engine.currentMission;
                                remainingTimeLabel.updateTime(engine.missionDuration);
                            }
                        }

                        Connections {
                            target: remainingTimeLabel.mission
                            onRemainingTimeChanged: {
                                remainingTimeLabel.updateTime(remainingTimeLabel.mission.remainingTime);
                            }
                        }
                    }
                }
            }

            GridLayout {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: 0.0125 * infoColumn.height
                rowSpacing: 0.0125 * infoColumn.height

                Label {
                    text: "Keys:"
                    font.bold: true
                    font.pixelSize: 0.03 * infoColumn.height
                    Layout.row: 0
                    Layout.column: 0
                    Layout.columnSpan: 2
                }

                Label {
                    text: "B G Y R"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 1
                    Layout.column: 0
                }

                Label {
                    text: "Select robot"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 1
                    Layout.column: 1
                }

                Label {
                    text: "↑ ↓ ← →"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 2
                    Layout.column: 0
                }

                Label {
                    text: "Move robot"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 2
                    Layout.column: 1
                }

                Label {
                    text: "U"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 3
                    Layout.column: 0
                }

                Label {
                    text: "Undo move"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 3
                    Layout.column: 1
                }

                Label {
                    text: "S"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 4
                    Layout.column: 0
                }

                Label {
                    text: "Reset robots"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 4
                    Layout.column: 1
                }

                Label {
                    text: "Ctrl+C"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 5
                    Layout.column: 0
                    Layout.topMargin: 0.025 * infoColumn.height
                }

                // TODO: enable this button only when AI have found a solution
                Label {
                    text: "Complete mission"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 5
                    Layout.column: 1
                    Layout.topMargin: 0.025 * infoColumn.height
                }

                Label {
                    text: "Ctrl+N"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 6
                    Layout.column: 0
                }

                Label {
                    text: "New game"
                    font.bold: true
                    font.pixelSize: 0.025 * infoColumn.height
                    Layout.row: 6
                    Layout.column: 1
                }
            }
        }

        GameView {
            id: gameView
            player: engine.userPlayer;
            Layout.row: 0
            Layout.column: 0
            Layout.fillHeight: true
            Layout.preferredWidth: height
        }
    }
}
