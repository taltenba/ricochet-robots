#ifndef TARGET_H
#define TARGET_H

#include <state.h>
#include <color.h>

namespace Game {
    /**
     * @brief Represents a target on the game board
     */
    class Target
    {
        Q_GADGET

        Q_PROPERTY(Game::BoardPos pos READ getPos CONSTANT)
        Q_PROPERTY(Game::Color color READ getColor CONSTANT)

    public:
        Target();
        Target(int x, int y, Color color);
        Target(BoardPos pos, Color color);

        BoardPos getPos() const;

        Color getColor() const;

        bool isReached(State state) const;

    private:
        Color mColor;
        BoardPos mPos;
    };
}

Q_DECLARE_METATYPE(Game::Target)

#endif // TARGET_H
