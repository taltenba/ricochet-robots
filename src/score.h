#ifndef SCORE_H
#define SCORE_H

#include <QObject>

namespace Game {
    /**
     * @brief Represents the score of the players
     */
    class Score
    {
        Q_GADGET

    public:
        Score();

        static constexpr int PLAYER_COUNT = 2;

        enum class Player
        {
            USER = 0,
            COMPUTER
        };
        Q_ENUM(Player)

        Q_INVOKABLE
        int getPoints(Player player) const;

        void addPoint(Player player);

        void reset();

    private:
        int mPoints[PLAYER_COUNT];
    };
}

Q_DECLARE_METATYPE(Game::Score)
Q_DECLARE_METATYPE(Game::Score::Player)

#endif // SCORE_H
