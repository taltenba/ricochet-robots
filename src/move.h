#ifndef MOVE_H
#define MOVE_H

#include <QObject>
#include <QPoint>
#include <color.h>
#include <robot.h>
#include <boardsquare.h>
#include <boardpos.h>

namespace Game {
    /**
     * @brief Represents a move.
     */
    class Move
    {
        Q_GADGET

    public:
        static constexpr int DIRECTION_COUNT = 4;

        enum class Direction {
            LEFT = 0,
            UP,
            RIGHT,
            DOWN
        };
        Q_ENUM(Direction)

        Move();
        Move(Robot mRobot, Direction dir);

        Robot getRobot() const;

        Direction getDirection() const;

        QPoint getVector() const;

        BoardSquare::Wall getBlockingWall() const;

        static QPoint getVector(Direction dir);

        static BoardSquare::Wall getBlockingWall(Direction dir);

        //Move getOpposite() const;

    private:
        Robot mRobot;
        Direction mDirection;
    };
}

Q_DECLARE_METATYPE(Game::Move)
Q_DECLARE_METATYPE(Game::Move::Direction)

#endif // MOVE_H
