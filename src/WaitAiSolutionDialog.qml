import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import com.arp.ricochetrobots.Solver 1.0

Dialog {
    title: qsTr("Completing mission")
    standardButtons: Dialog.Cancel

    QtObject {
        id: internal
        property var ai: null
    }

    function openDialog(ai) {
        internal.ai = ai;
        open();
    }

    onClosed: internal.ai = null

    Connections {
        target: internal.ai
        onThink: accept()
        onFinished: accept()
    }

    Label {
        text: qsTr("Please wait for AI's solution...")
    }
}
