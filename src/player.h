#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QVector>
#include <QStack>
#include <missioncontext.h>
#include <move.h>

namespace Game {
    /**
     * @brief Represents a player, allowing the user to move robots and keeping
     *        the history of made moves.
     */
    class Player : public QObject
    {
        Q_OBJECT

        Q_PROPERTY(Game::MissionContext missionContext READ getMissionContext CONSTANT)
        Q_PROPERTY(Game::State gameState READ getGameState NOTIFY gameStateChanged)
        Q_PROPERTY(int bestSolutionLength READ getBestSolutionLength NOTIFY bestSolutionChanged)
        Q_PROPERTY(int moveCount READ getMoveCount NOTIFY gameStateChanged)

    public:
        Player(MissionContext const& mission, QObject* parent = nullptr);

        void play(Move const& move);

        Q_INVOKABLE
        void play(Game::Robot robot, Game::Move::Direction moveDir);

        Q_INVOKABLE
        void undoLastMove();

        Q_INVOKABLE
        void undoAllMoves();

        Q_INVOKABLE
        bool canUndoMove() const;

        bool hasFoundSolution() const;

        MissionContext const& getMissionContext() const;

        State getGameState() const;

        QVector<State> getBestSolution() const;

        int getBestSolutionLength() const;

        int getMoveCount() const;

    signals:
        void gameStateChanged(State newState, int moveCount);

        void bestSolutionChanged(int newLength);

    private:
        MissionContext const& mMission;
        QStack<State> mMoveHistory;
        QVector<State> mBestSolution;
        State mGameState;
    };
}

#endif // PLAYER_H
