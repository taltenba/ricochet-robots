import QtQuick 2.12
import QtQuick.Controls 2.12
import com.arp.ricochetrobots.GameBoard 1.0;
import com.arp.ricochetrobots.BoardPos 1.0
import com.arp.ricochetrobots.GameColor 1.0;
import com.arp.ricochetrobots.BoardSquare 1.0;
import com.arp.ricochetrobots.MissionContext 1.0
import com.arp.ricochetrobots.Target 1.0
import com.arp.ricochetrobots.Robot 1.0;
import com.arp.ricochetrobots.State 1.0;
import com.arp.ricochetrobots.Move 1.0;
import com.arp.ricochetrobots.Player 1.0;
import com.arp.ricochetrobots.Solver 1.0;

Item {
    id: gameView
    focus: player != null

    readonly property int boardSize: 16;
    readonly property real squareSize: width / boardSize;

    property Player player: null;
    property var missionContext: null;
    property var gameState: null;
    property int bestSolutionLength: 0;
    property int moveCount: 0;

    QtObject {
        id: internal

        property var selectedRobot: null;
    }

    onMissionContextChanged: internal.selectedRobot = null;

    MouseArea {
        anchors.fill: parent
        enabled: player != null && gameState != null
        onClicked: {
            var squareX = Math.floor(mouse.x / squareSize);
            var squareY = Math.floor(mouse.y / squareSize);

            internal.selectedRobot = gameState.findWrappedRobot(squareX, squareY);
        }
    }

    function moveSelectedRobot(direction) {
        if (internal.selectedRobot == null)
            return;

        player.play(internal.selectedRobot, direction);
    }

    Keys.enabled: player != null && gameState != null

    Keys.onLeftPressed: moveSelectedRobot(Move.LEFT)
    Keys.onUpPressed: moveSelectedRobot(Move.UP)
    Keys.onRightPressed: moveSelectedRobot(Move.RIGHT)
    Keys.onDownPressed: moveSelectedRobot(Move.DOWN)

    Keys.onPressed: {
        switch (event.key) {
            case Qt.Key_B:
                internal.selectedRobot = ROBOTS[GameColor.BLUE];
                break;
            case Qt.Key_R:
                internal.selectedRobot = ROBOTS[GameColor.RED];
                break;
            case Qt.Key_G:
                internal.selectedRobot = ROBOTS[GameColor.GREEN];
                break;
            case Qt.Key_Y:
                internal.selectedRobot = ROBOTS[GameColor.YELLOW];
                break;
            case Qt.Key_U:
                if (player.canUndoMove())
                    player.undoLastMove();
                break;
            case Qt.Key_S:
                player.undoAllMoves();
                break;
        }
    }

    Connections {
        target: player
        onBestSolutionChanged: {
            gameView.moveCount = 0;
            gameView.bestSolutionLength = newLength;
        }
        onGameStateChanged: {
            gameView.gameState = player.gameState;
            gameView.moveCount = player.moveCount;
        }
    }

    onPlayerChanged: {
        if (!player) {
            missionContext = null;
            gameState = null;
            bestSolutionLength = 0;
            moveCount = 0;
        } else {
            missionContext = player.missionContext;
            gameState = player.gameState;
            bestSolutionLength = player.bestSolutionLength;
            moveCount = player.moveCount;
        }
    }

    Canvas {
        anchors.fill: parent

        onPaint: {
            var boardSize = gameView.boardSize;
            var squareSize = gameView.squareSize;
            var ctx = getContext("2d");

            ctx.lineWidth = 0.4;
            ctx.strokeStyle = "whitesmoke";

            ctx.beginPath();

            for (var i = 0; i <= boardSize; ++i) {
                ctx.moveTo(i * squareSize, 0);
                ctx.lineTo(i * squareSize, height);
                ctx.moveTo(0, i * squareSize);
                ctx.lineTo(width, i * squareSize);
            }

            ctx.stroke();
        }
    }

    function toQmlColor(gameColor) {
        switch (Number(gameColor)) {
            case GameColor.RED:
                return "tomato";
            case GameColor.BLUE:
                return "dodgerblue";
            case GameColor.GREEN:
                return "yellowgreen";
            case GameColor.YELLOW:
                return "gold";
            default:
                console.assert(false, "Invalid color " + gameColor);
        }
    }

    Canvas {
        id: boardCanvas
        anchors.fill: parent

        function drawTarget(ctx) {
            var target = missionContext.target;
            var targetX = target.pos.x * squareSize;
            var targetY = target.pos.y * squareSize;

            if (Number(target.color) === GameColor.MULTICOLOR) {
                var targetCenterX = targetX + 0.5 * squareSize;
                var targetCenterY = targetY + 0.5 * squareSize;

                var grad = ctx.createRadialGradient(targetCenterX, targetCenterY,
                                                    squareSize * 0.05, targetCenterX, targetCenterY,
                                                    0.6 * squareSize);
                grad.addColorStop(0.00, gameView.toQmlColor(GameColor.BLUE));
                grad.addColorStop(0.33, gameView.toQmlColor(GameColor.GREEN));
                grad.addColorStop(0.66, gameView.toQmlColor(GameColor.YELLOW));
                grad.addColorStop(1.00, gameView.toQmlColor(GameColor.RED));

                ctx.fillStyle = grad;
            } else {
                ctx.fillStyle = gameView.toQmlColor(target.color);
            }

            ctx.beginPath();
            ctx.rect(targetX, targetY, squareSize, squareSize);
            ctx.fill();

            ctx.strokeStyle = "whitesmoke";
            ctx.lineWidth = 0.4;
            ctx.stroke();
        }

        function drawBoard(ctx) {
            var board = missionContext.board;

            ctx.lineWidth = 7;
            ctx.lineCap  = "round";
            ctx.strokeStyle = "whitesmoke";

            ctx.beginPath();

            for (var i = 0; i < boardSize; ++i) {
                var xStart = i * squareSize;
                var xEnd = xStart + squareSize;

                for (var j = 0; j < boardSize; ++j) {
                    var yStart = j * squareSize;
                    var yEnd = yStart + squareSize;

                    var square = board.getWrappedSquare(i, j);

                    if (square.hasWall(BoardSquare.LEFT)) {
                        ctx.moveTo(xStart, yStart);
                        ctx.lineTo(xStart, yEnd);
                    }

                    if (square.hasWall(BoardSquare.BOTTOM)) {
                        ctx.moveTo(xStart, yEnd);
                        ctx.lineTo(xEnd, yEnd);
                    }

                    if (square.hasWall(BoardSquare.RIGHT)) {
                        ctx.moveTo(xEnd, yEnd);
                        ctx.lineTo(xEnd, yStart);
                    }

                    if (square.hasWall(BoardSquare.TOP)) {
                        ctx.moveTo(xEnd, yStart);
                        ctx.lineTo(xStart, yStart);
                    }
                }
            }

            ctx.stroke();

            ctx.beginPath();
            ctx.fillStyle = "#555555";
            ctx.rect(7 * squareSize, 7 * squareSize, 2 * squareSize, 2 * squareSize);
            ctx.fill();
        }

        onPaint: {
            var ctx = getContext("2d");
            ctx.reset();

            if (!missionContext)
                return;

            drawTarget(ctx);
            drawBoard(ctx);
        }

        Connections {
            target: gameView
            onMissionContextChanged: boardCanvas.requestPaint()
        }
    }

    Canvas {
        id: foregroundCanvas
        anchors.fill: parent

        onPaint: {
            var ctx = getContext("2d");
            ctx.reset();

            if (!gameState)
                return;

            var pos;

            if (internal.selectedRobot) {
                pos = gameState.getRobotPos(internal.selectedRobot);

                ctx.beginPath();
                ctx.ellipse((pos.x + 0.05) * squareSize, (pos.y + 0.05) * squareSize, 0.9 * squareSize, 0.9 * squareSize);
                ctx.strokeStyle = "red";
                ctx.lineWidth = 2;
                ctx.stroke();
            }

            ctx.strokeStyle = "whitesmoke";
            ctx.lineWidth = 1.0;

            for (var i = 0; i < ROBOTS.length; ++i) {
                var robot = ROBOTS[i];

                pos = gameState.getRobotPos(robot);

                ctx.beginPath();
                ctx.ellipse((pos.x + 0.15) * squareSize, (pos.y + 0.15) * squareSize, 0.7 * squareSize, 0.7 * squareSize);
                ctx.fillStyle = gameView.toQmlColor(robot.color);
                ctx.fill();
                ctx.stroke();
            }
        }

        Connections {
            target: gameView
            onGameStateChanged: foregroundCanvas.requestPaint();
        }

        Connections {
            target: internal
            onSelectedRobotChanged: foregroundCanvas.requestPaint();
        }
    }

    Label {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: gameView.squareSize * 0.10
        anchors.leftMargin: gameView.squareSize * 0.15
        text: moveCount
        font.pixelSize: gameView.squareSize * 0.4
        visible: missionContext != null
    }

    Column {
        height: 2 * gameView.squareSize
        width: 2 * gameView.squareSize
        anchors.centerIn: parent
        visible: missionContext != null

        Label {
            anchors.right: parent.right
            anchors.left: parent.left
            height: gameView.squareSize * 0.8
            text: "Best"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
            font.pixelSize: gameView.squareSize * 0.5
        }

        Label {
            id: bestSolutionLabel
            anchors.right: parent.right
            anchors.left: parent.left
            height: squareSize * 1.2
            text: "-"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignTop
            font.pixelSize: squareSize * 0.8

            Connections {
                target: gameView
                onBestSolutionLengthChanged: {
                    if (bestSolutionLength > 0)
                        bestSolutionLabel.text = bestSolutionLength;
                    else
                        bestSolutionLabel.text = "-";
                }
            }
        }
    }
}
