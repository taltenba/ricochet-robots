#include <move.h>

using namespace Game;

Move::Move() : mRobot(), mDirection()
{
    // Empty
}

Move::Move(Robot robot, Direction dir) : mRobot(robot), mDirection(dir)
{
    // Empty
}

Robot Move::getRobot() const
{
    return mRobot;
}

Move::Direction Move::getDirection() const
{
    return mDirection;
}

QPoint Move::getVector() const
{
    return getVector(mDirection);
}

QPoint Move::getVector(Direction dir)
{
    switch (dir) {
        case Move::Direction::UP:
            return QPoint(0, -1);
        case Move::Direction::LEFT:
            return QPoint(-1, 0);
        case Move::Direction::RIGHT:
            return QPoint(1, 0);
        case Move::Direction::DOWN:
            return QPoint(0, 1);
    }

    Q_ASSERT(false);
    return {};
}

BoardSquare::Wall Move::getBlockingWall() const
{
    return getBlockingWall(mDirection);
}

BoardSquare::Wall Move::getBlockingWall(Direction dir)
{
    switch (dir) {
        case Move::Direction::UP:
            return BoardSquare::Wall::TOP;
        case Move::Direction::LEFT:
            return BoardSquare::Wall::LEFT;
        case Move::Direction::RIGHT:
            return BoardSquare::Wall::RIGHT;
        case Move::Direction::DOWN:
            return BoardSquare::Wall::BOTTOM;
    }

    Q_ASSERT(false);
    return {};
}

/*Move Move::getOpposite() const
{
    switch (mDirection)
    {
        case Direction::UP:
            return Move(mRobot, Direction::DOWN);
        case Direction::LEFT:
            return Move(mRobot, Direction::RIGHT);
        case Direction::RIGHT:
            return Move(mRobot, Direction::LEFT);
        case Direction::DOWN:
            return Move(mRobot, Direction::UP);
    }

    Q_ASSERT(false);
}*/
