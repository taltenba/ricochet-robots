#ifndef BOARD_H
#define BOARD_H

#include <QObject>
#include <QPoint>
#include <QSet>
#include <QVector>
#include <QVariant>
#include <boardsquare.h>
#include <move.h>
#include <boardpos.h>

namespace Game {
    /**
     * @brief Represents the game board.
     */
    class Board
    {
        Q_GADGET

    public:
        static constexpr int WIDTH = 16;
        static constexpr int HEIGHT = 16;

        Board();

        BoardSquare const& getSquare(BoardPos const& pos) const;

        Q_INVOKABLE
        QVariant getWrappedSquare(int x, int y) const;

        BoardSquare const (*getAllSquares() const)[HEIGHT];

        BoardPos getRandomTargetPos(QSet<BoardPos> const& excludedPos) const;

        BoardPos getRandomPos(QSet<BoardPos> const& excludedPos) const;

        BoardPos getPrecomputedMoveResult(BoardPos pos, Move::Direction moveDir) const;

        BoardSquare const* operator[](int row) const;

        BoardSquare* operator[](int row);

        void setValidTargetPos(QVector<BoardPos> const& pos);

        void precomputeMoves();

    private:
        BoardSquare mSquares[WIDTH][HEIGHT];
        BoardPos mMoveResults[WIDTH][HEIGHT][Move::DIRECTION_COUNT];
        QVector<BoardPos> mValidTargetPos;
    };
}

Q_DECLARE_METATYPE(Game::Board)

#endif // BOARD_H
