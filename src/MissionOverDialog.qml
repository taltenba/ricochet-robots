import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import com.arp.ricochetrobots.Move 1.0
import com.arp.ricochetrobots.MissionContext 1.0
import com.arp.ricochetrobots.State 1.0
import com.arp.ricochetrobots.GameEngine 1.0

Dialog {
    id: dialog
    title: qsTr("Mission over")
    contentHeight: 700
    contentWidth: 850

    property var aiSolutionStates: null;
    property var initState: null;

    function openDialog(userSolLength, aiSolStates, missionContext, missionResult) {
        switch (Number(missionResult)) {
            case GameEngine.COMPUTER_WINS:
                winnerLabel.text = qsTr("AI has found the best solution")
                break;
            case GameEngine.USER_WINS:
                winnerLabel.text = qsTr("You have found the best solution")
                break;
            case GameEngine.DRAW:
                winnerLabel.text = qsTr("Your solution and AI's one are equivalent")
                break;
            default:
                console.assert(false);
        }

        gameView.missionContext = missionContext;
        gameView.gameState = missionContext.initState;
        gameView.moveCount = 0;
        gameView.bestSolutionLength = aiSolStates.length - 1;

        dialog.aiSolutionStates = aiSolStates;
        dialog.initState = missionContext.initState;

        if (aiSolutionStates.length > 1)
            playAISolutionTimer.start();

        open();
    }

    onClosed: playAISolutionTimer.stop();

    Timer {
        id: playAISolutionTimer
        interval: 500
        repeat: true

        onTriggered: {
            gameView.moveCount++;

            if (gameView.moveCount === aiSolutionStates.length) {
                gameView.moveCount = 0;
                gameView.gameState = aiSolutionStates[0];
            } else {
                gameView.gameState = aiSolutionStates[gameView.moveCount];
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 20

        Label {
            id: winnerLabel
            text: "You have found the best solution"
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            font.pixelSize: 40
            Layout.fillWidth: true
        }

        Label {
            text: "AI's solution:"
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: 24
            Layout.fillWidth: true
        }

        GameView {
            id: gameView
            width: gameView.heigth
            Layout.fillHeight: true
            Layout.preferredWidth: height
            Layout.alignment: Qt.AlignHCenter
        }
    }
}
