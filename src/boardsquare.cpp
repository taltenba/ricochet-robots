#include "boardsquare.h"

using namespace Game;

BoardSquare::BoardSquare() : mWalls()
{
    // Empty
}

BoardSquare::BoardSquare(quint8 walls) : mWalls(walls)
{
    // Empty
}

bool BoardSquare::hasWall(Wall wall) const
{
    return mWalls & static_cast<quint8>(wall);
}

void BoardSquare::addWall(Wall wall)
{
    mWalls |= static_cast<quint8>(wall);
}

void BoardSquare::removeWall(Wall wall)
{
    mWalls &= ~static_cast<quint8>(wall);
}

BoardSquare BoardSquare::rotate(int rot) const
{
    // Performs circular bit rotation
    int n = rot / 90;
    quint8 newWalls = static_cast<quint8>((mWalls << n) | (mWalls >> (4 - n)));

    return BoardSquare(newWalls);
}
