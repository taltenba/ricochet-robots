#include "target.h"
#include "robot.h"

using namespace Game;

Target::Target() : mColor(), mPos()
{
    // Empty
}

Target::Target(int x, int y, Color color) : mColor(color), mPos(x, y)
{
    // Empty
}

Target::Target(BoardPos pos, Color color) : mColor(color), mPos(pos)
{
    // Empty
}

BoardPos Target::getPos() const
{
    return mPos;
}

Color Target::getColor() const
{
    return mColor;
}

bool Target::isReached(State state) const
{
    if (mColor == Color::MULTICOLOR)
    {
        for (int i = 0; i < Robot::ROBOT_COUNT; ++i)
        {
            if (state.getRobotPos(Robot::ROBOTS[i]) == mPos)
                return true;
        }

        return false;
    }

    return state.getRobotPos(Robot(mColor)) == mPos;
}
