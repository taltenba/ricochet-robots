#ifndef SOLVERSTATS_H
#define SOLVERSTATS_H

#include <QVector>
#include <QTime>
#include <QDebug>
#include <iostream>

#include "solver.h"
#include "board.h"
#include "missioncontext.h"
#include "randomboardgenerator.h"

namespace Game {

struct Stat{
    int missionIndex;
    int solution;
    int msTime;
    bool interrupted;
    int maxMsTime;
    State initState;
    Target target;
    Solver::DifficultyLevel difficulty;
    QString solverName;
};

class SolverStats : public QObject
{

    Q_OBJECT

public:
    SolverStats(Solver &solver);

    void startSimulation();
    void startSimulation(int nbSimulation);
    void startSimulation(QVector<MissionContext> const& poolTest);

    void randomPoolTest(int poolSize);

    QVector<Stat> stats() const;

    friend std::ostream& operator<<(std::ostream &o, SolverStats const& st);

    QVector<MissionContext> poolTest() const;
    void setPoolTest(const QVector<MissionContext> &poolTest);

    QVector<Board> boards() const;
    void setBoards(const QVector<Board> &boards);

private slots:
    void onFinished(bool isTimeOut);
    void onThink(QVector<Game::State> solution);

private:
    int mMissionIndex;
    Solver& mSolver;
    QTime mTime;
    QVector<Stat> mStats;
    QVector<State> mBestSolution;
    MissionContext mCurrentMissionContext;
    QVector<MissionContext> mPoolTest;
    QVector<Board> mBoards;
};

}


#endif // SOLVERSTATS_H
