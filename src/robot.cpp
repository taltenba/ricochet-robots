#include <robot.h>

using namespace Game;

// Must be indexed by Game::Color
const Robot Robot::ROBOTS[ROBOT_COUNT] =
        {
            Robot(Color::RED),
            Robot(Color::YELLOW),
            Robot(Color::BLUE),
            Robot(Color::GREEN)
        };

Robot::Robot() : mColor()
{
    // Empty
}

Robot::Robot(Color color) : mColor(color)
{
    Q_ASSERT(color != Color::MULTICOLOR);
}

Color Robot::getColor() const
{
    return mColor;
}
